import lxml.etree as ET
import re
import os
import xlsxwriter
import json
import traceback
from utils.logger import Logger

env = os.environ

ROOT_DIR = env.get("ROOT_DIR")
RESULT_DIR = env.get("RESULT_DIR")

class SpreadSheetParser(Logger):
    """
    Класс для парсинга Excel Spread Sheet
    """
    pattern_msn = r'\{urn:schemas-microsoft-com:office:.*\}'
    prefix = '{urn:schemas-microsoft-com:office:spreadsheet}'

    def __init__(self, filename):
        super().__init__()
        
        self.filename = filename
        self.path_to_save = None

    def get_attr(self, element, name):
        return element.attrib.get(f'{self.prefix}{name}')

    def prepare_dir(self):
        """
        Preparing dir
        :return:
        """
        dir_to_save = os.path.dirname(self.path_to_save).replace(ROOT_DIR, RESULT_DIR)
        if not os.path.exists(dir_to_save):
            os.makedirs(dir_to_save)

    def get_name_to_save(self, path, sheet_title):
        """
        Возвращает имя файла, в который будет сохранен результат
        :param path:
        :param sheet_title:
        :return:
        """
        dir_to_save = os.path.dirname(path).replace(ROOT_DIR, RESULT_DIR)

        new_path = os.path.join(dir_to_save, os.path.basename(path))
        pre, ext = os.path.splitext(new_path)
        p = f'{pre}_json'
        if not os.path.exists(p):
            os.makedirs(p)

        path_to_save = os.path.join(f'{pre}_json', f'{sheet_title}.json')

        self.path_to_save = path_to_save

    def parse(self):
        """
        Парсинг, основная функция
        :return:
        """
        self.get_name_to_save(self.filename, '')
        
        root = self.path_to_save.rsplit('/', 1)[0]
        if os.listdir(root):
            self.log.warning(f'folder is not empty yet')
            return 

        current_row = {}
        current_cell_coord = None
        current_row_index = 0
        f = None
        merge_acros = 0

        try:
            for event, element in ET.iterparse(self.filename, events=("start", "end")):
                tag = re.sub(self.pattern_msn, '', element.tag)

                if event == "start":
                    if tag == "Worksheet":
                        sheet_name = self.get_attr(element, 'Name')
                        self.get_name_to_save(self.filename, sheet_name)
                        if f is not None:
                            f.close()

                        f = open(self.path_to_save, 'a+')
                        self.log.info(f'Sheet name: {sheet_name}')

                    if tag == 'Row':
                        current_row_index += 1
                        if current_row_index == 47:
                            a = 1

                        if current_row_index % 100 == 0:
                            self.log.info(f'row {current_row_index}:')
                        if current_row_index % 10 == 0:
                            print('.', end='')

                    if tag == 'Cell':
                        key_ind = self.get_attr(element, 'Index')
                        if key_ind is not None:
                            current_cell_index = int(key_ind)
                        else:
                            if merge_acros is not None:
                                current_cell_index += int(merge_acros)
                            current_cell_index += 1

                        letter = xlsxwriter.utility.xl_col_to_name(current_cell_index - 1)
                        current_cell_coord = f'{letter}{current_row_index}'

                        merge_acros = self.get_attr(element, 'MergeAcross')

                    if tag == 'Data':
                        _type = self.get_attr(element, 'Type')
                        if _type:
                            value = None
                            if _type in ['String', 'Number', 'DateTime']:
                                value = element.text
                            else:
                                a = 1

                            if value is not  None:
                                current_row[current_cell_coord] = value

                elif event == 'end':
                    if tag == 'Row' and current_row:
                        row = json.dumps(current_row, ensure_ascii=False)
                        f.write(f'{row}\n')

                        current_row = {}
                        merge_acros = current_cell_index = 0

                        element.clear()
                        while element.getprevious() is not None:
                            del element.getparent()[0]

                    if tag == "Worksheet":
                        f.write(f'END_FILE')

        except Exception as e:
            traceback.print_exc()
            self.log.error(e)

        finally:
            if f is not None:
                f.close()


if __name__ == '__main__':
    p = '/home/d/data/rosneft/29. РН-Бурение/Сахалинский филиал РНБ/Прочее/БФ №ИНВ-6 Акт инв-ции ТМЦ в пути.xls'
    parser = SpreadSheetParser(p)
    parser.parse()