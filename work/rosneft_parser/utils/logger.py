from functools import wraps
import traceback
import logging
import os


FORMAT = '%(asctime)s: %(levelname)s %(funcName)3s(): %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)

class Logger:
    """
    Logger
    """
    def __init__(self):
        self.log = logging.getLogger(__name__)

    def add_file_handler(self, filename, level):
        # -------------------
        fh = logging.FileHandler(filename=filename)
        fh.setLevel(level)

        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # add formatter to ch
        fh.setFormatter(formatter)
        self.log.addHandler(fh)

    def wrap_error(f):
        @wraps(f)
        def wrapped(inst, *args, **kwargs):
            try:
                return f(inst, *args, **kwargs)
            except Exception as e:
                inst.is_valid = False
                inst.log.error(f'__{f.__name__}(): {e}')
                inst.log.error(f'__{f.__name__}(): {traceback.print_exc()}')

        return wrapped