import xlrd
from openpyxl.workbook import Workbook
import os
import glob
import shutil
import xlsxwriter
import sys, traceback
from spread_sheet_parser import SpreadSheetParser

MAX_EMPTY_ROWS = 200

def get_data_from_xls(filename, only_sheet_name=False):
    """
    Возвращает данные из xls
    :param filename:
    :param only_sheet_name: if True - return only sheet name from book
    :return:
    """
    book_result = {}

    # first open using xlrd
    print('Read from xls')

    try:
        with xlrd.open_workbook(filename, on_demand=True) as book:

            if only_sheet_name:
                sheet_names = [i.name for i in book.sheets()]
                return sheet_names

            for sheet in book.sheets():
                print(f'Convert sheet {sheet.name}')

                nrows, ncols = sheet.nrows, sheet.ncols
                print(f'nrows={nrows}')
                print(f'ncols={ncols}')

                print('')

                # prepare a xlsx sheet
                sheet_res = []

                print(f'Create custom book')

                is_empty_row_count = 0

                for row in range(0, nrows):
                    row_res = {}

                    try:
                        row_slice = sheet.row_slice(row)
                        if all([sheet.cell_type(row, i) == 0 for i, cell in enumerate(row_slice)]):
                            is_empty_row_count += 1
                            continue
                        else:
                            is_empty_row_count = 0

                        if is_empty_row_count > MAX_EMPTY_ROWS:
                            break

                    except Exception as e:
                        traceback.print_exc()
                        print(e)

                    if row % 1000 == 0:
                        print(f'.', end='.')

                    try:
                        for col in range(0, ncols):
                            cell = sheet.cell(row, col)

                            # if date
                            if cell.ctype == xlrd.XL_CELL_DATE:
                                _date = xlrd.xldate_as_tuple(cell.value, book.datemode)
                                if len(_date) > 3:
                                    value = "{:02}.{:02}.{:04}".format(_date[2], _date[1], _date[0])
                                else:
                                    value = None
                            else:
                                value = cell.value

                            # print(f'value: {value}')

                            try:
                                if len(str(value)) == 0 or cell.ctype == 5:
                                    continue

                                letter = xlsxwriter.utility.xl_col_to_name(col)
                                coordinate = f'{letter}{row+1}'
                                row_res[coordinate] = value

                            except Exception as e:
                                print(e)

                    except Exception as e:
                        traceback.print_exc()
                        print(e)

                    if row_res:
                        sheet_res.append(row_res)
                # ----------------------
                sheet_res.append('END_FILE')
                book_result[sheet.name] = sheet_res

    except xlrd.biffh.XLRDError as e:
        try:
            print('Try parsing by SpreadSheetParser')
            parser = SpreadSheetParser(filename)
            parser.parse()
            print('Success!')
            return
        except Exception as e:
            traceback.print_exc()
            print(e)
            return 'Error'

    except Exception as e:
        traceback.print_exc()
        print(e)
        return 'Error'

    return book_result


def delete_folder_by_mask(path):
    """
    Delete folder by mask
    :param path: glob mask
    :return:
    """
    folderlist = glob.glob(path, recursive=True)

    for f in folderlist:
        print(f'Remove dir {f}')
        shutil.rmtree(f)