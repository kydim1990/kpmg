#!/usr/bin/env bash

export PYTHONPATH=.

#export ROOT_DIR=/home/d/data/rosneft_json_raw
export ROOT_DIR=/data/projects/Rosneft/rosneft_json_raw
#export RESULT_DIR=/home/d/data/rosneft_json
export RESULT_DIR=/data/projects/Rosneft/rosneft_json

export IS_WRITE_LOG=1

find . -name "*_${1}.log" -type f -delete

PART_TO_PARSE=${2:-/}
echo ${PART_TO_PARSE}


python processor/main_proccessor_raw_json.py -dep $1 -ptp "${PART_TO_PARSE}"