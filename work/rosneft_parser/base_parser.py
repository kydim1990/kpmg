import glob
import openpyxl
from openpyxl.utils import get_column_letter
from config import ROOT_DIR, RESULT_DIR, IS_OVERWRITE, IS_WRITE_LOG
from utils.logger import Logger
import os
import re
import json
from datetime import datetime
import time
import logging
import traceback

class Parser(Logger):
    """
    Базовый класс парсера
    """

    def __init__(self, path, config, path_to_parse=None, dep_code=None):
        super().__init__()

        self.file_list = glob.glob(path, recursive=True)
        self.config = config
        self.path_to_parse = path_to_parse
        self.data = {
            'rows': [],
            'header': {},
            'footer': {},
        }
        self.path_to_save = None
        self.expansion = None
        self.min_valid_date = datetime(1984, 5, 17)

        if IS_WRITE_LOG:
            logs_to_file = {
                f'logs/warning_{dep_code}.log': logging.WARNING,
                f'logs/error_{dep_code}.log': logging.ERROR,
            }

            for k, v in logs_to_file.items():
                path = os.path.join(os.path.dirname(__file__), k)
                self.add_file_handler(path, level=v)

    def parsing_all(self):
        """
        Запускает процедуру слияния данных для self.file_list
        :return:
        """
        for i, f in enumerate(self.file_list):
            self.get_name_to_save(f)

            if os.path.isfile(self.path_to_save) and not IS_OVERWRITE:
                self.log.info(f'Skip file {f}')
                continue

            self.log.info(f"{i}: Processing file {f}")
            self.log.info(f'start_time: {time.strftime("%H:%M")}')
            parse_result = self.parsing_one(f)
            if parse_result == "error":
                self.log.error("Error during parsing")
                continue

            self.save()
            self.log.info(f'end_time: {time.strftime("%H:%M")}')
            self.log.info(f"End processing file {f}")
            self.log.info(f"-----------------------")

    def get_book(self, path):
        """
        Возвращает book как объект openpyxl.workbook
        :return: workbook
        """
        book = None
        try:
            book = openpyxl.load_workbook(path, read_only=True, keep_vba=False, guess_types=False, data_only=True)
        except Exception as e:
            traceback.print_exc()
            self.log.error(e)

        return book

    def get_sheet(self, book):
        """
        Возвращает sheet из book
        :param book:
        :return: Sheet from book
        """
        sheet = None
        wb_titles = [i.title for i in book.worksheets]
        for i in self.config["sheets"]:
            if i in wb_titles:
                sheet = book[i]
                break
        else:
            raise Exception("Wrong sheet name")

        return sheet

    def parsing_one(self, path):
        """
        Запускает процедуру слияния данных для одного файла
        :param f:
        :return:
        """
        self.log.info(f"reading file...")
        self.data['src'] = path

        book = self.get_book(path)

        try:
            sheet = self.get_sheet(book)
            self.get_data_header(sheet)
            self.get_rows(sheet.rows)

        except Exception as e:
            self.log.error(e)
            return 'error'
        finally:
            book.close()

    def get_data_header(self, sheet):
        """
        Возвращает заголовки для описей
        :param sheet:
        :return: Header for registers
        """
        self.log.info(f"Get header...")
        data = {}
        try:
            for k, v in self.config['data_header'].items():
                data[k] = {
                    'title': v['title'],
                    'value': sheet[v['cell']].value
                }

        except Exception as e:
            self.log.error(e)

        self.data["data_header"] = data
        self.log.info(f"End get header")

    def get_cell_value(self, cell, is_for_hash=False):
        """
        Возвращает значение из ячейки
        :param cell:
        :param is_for_hash:
        :return: value from cell
        """
        if cell.value is None:
            return

        result = None

        try:
            if cell.is_date:
                if cell.value < self.min_valid_date:
                    result = str(cell.internal_value)
                else:
                    result = str(cell.value)
            else:
                if is_for_hash:
                    if type(cell.value) is float:
                        result = "{:.0f}".format(cell.value)
                result = str(cell.value)
        except Exception as e:
            self.log.error(e)
            raise

        if result == 'None':
            result = None

        return result

    def get_hash_row(self, row):
        """
        Возвращает hash для jsonline
        :param row:
        :return:
        """
        try:
            row_values = [
                self.get_cell_value(cell, is_for_hash=True)
                for cell in row if cell.value is not None]

            hash_row = ''.join(row_values)
            hash_row = hash_row.replace('\n', '').strip()
            # print(f'hash_row= {hash_row}')

            return hash_row
        except Exception as e:
            self.log.error(e)

    def get_letter(self, cell):
        """
        Возвращает букву из ячейки
        :param cell:
        :return: letter from cell
        """
        if self.expansion == 'xls':
            return cell.column
        else:
            return get_column_letter(cell.column)

    def get_rows(self, excel_rows):
        """
        Возвращает данные по описи из таблицы
        :param excel_rows:
        :return: rows
        """
        self.log.info(f"Get row data...")
        config_ = self.config['rows']

        result_rows = []
        try:
            is_record = False
            headers = config_['headers']
            for i, row in enumerate(excel_rows):
                data = {}

                hash_row = self.get_hash_row(row)

                if config_["row"].get('hash_break_record') is not None and\
                        re.match(config_["row"].get('hash_break_record'), hash_row):
                    break

                is_record = is_record and not re.match(config_["row"]['hash_stop_record'], hash_row)

                if is_record:
                    cells = (cell for cell in row if cell.value is not None)

                    for cell in cells:
                        letter = self.get_letter(cell)
                        if letter in headers:
                            h = headers[letter]

                            data[h['code']] = {
                                'title': h['title'],
                                'value': self.get_cell_value(cell)
                            }

                    if data:
                        result_rows.append(data)

                is_record = is_record or re.match(config_["row"]['hash_start_record'], hash_row) is not None

        except Exception as e:
            self.log.error(e)

        self.data["rows"] = result_rows
        self.log.info(f"End get row data")

    def get_name_to_save(self, path):
        """
        Возвращает имя файла, в который будет сохранен результат
        :param path:
        :param sheet_title:
        :return:
        """
        dir_to_save = os.path.dirname(path).replace(ROOT_DIR, RESULT_DIR)
        if not os.path.exists(dir_to_save):
            os.makedirs(dir_to_save)

        new_path = os.path.join(dir_to_save, os.path.basename(path))
        pre, ext = os.path.splitext(new_path)
        path_to_save = f'{pre}.json'
        self.path_to_save = path_to_save

    def save(self):
        """
        Сохраняет результат в файл
        :return: save result to file
        """
        if len(self.data['rows']) == 0:
            self.log.info("!! rows_is_empty !!")

        self.log.info(f"Saving ...")

        self.log.info(f"Save result to file {self.path_to_save}")

        try:
            with open(self.path_to_save, "w") as f:
                json.dump(self.data, f, ensure_ascii=False, indent=4)
        except Exception as e:
            self.log.error(e)

        self.data = {
            'rows': [],
            'header': {},
            'footer': {}
        }
        self.log.info(f"End saving result to file {self.path_to_save}")