#!/usr/bin/env bash

export PYTHONPATH=.

export IS_OVERWRITE=0
export DEPS_STR=2,3,6,8,10,17,19,22,24,25,26,28,29,30,31
#export ROOT_DIR=/home/d/projects/rosneft_parser/tests/resources/process_raw/input
export ROOT_DIR=tests/resources/process_raw/input

export RESULT_DIR=tests/resources/process_raw/output

pytest tests/test_processor_raw_json.py --disable-pytest-warnings -v -vv