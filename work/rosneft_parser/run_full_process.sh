#!/usr/bin/env bash
IS_EXTRACT=${2:-0}
if [ "$IS_EXTRACT" = 1 ] ; then
    echo "extact data"
    bash run_extract_raw.sh $1
fi

bash run_processor_raw.sh $1
bash run_join_data.sh $1
bash run_remove_dirs_by_mask.sh $1