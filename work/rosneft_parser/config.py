import os
from department_config.og_14_knpz.config import CONFIG_14
from department_config.og_15_nzmp.config import CONFIG_15
from department_config.og_16_nnpo.config import CONFIG_16
from department_config.og_18_achinzki_npz.config import CONFIG_18
from department_config.og_19_yfaorg_intez.config import CONFIG_19
from department_config.og_2_yung.config import CONFIG_2
from department_config.og_3_rn_vankor.config import CONFIG_3
from department_config.og_27_bashneft_retail.config import CONFIG_27
from department_config.og_7_samolorneftgaz.config import CONFIG_7
from department_config.og_8_rospan_international.config import CONFIG_8
from department_config.og_22_kuban_oil_product.config import CONFIG_22
from department_config.og_23_rn_yamal_oil_product.config import CONFIG_23
from department_config.og_21_rn_moscow.config import CONFIG_21
from department_config.og_11_anhk.config import CONFIG_11
from department_config.og_20_rn_delivery.config import CONFIG_20
from department_config.og_29_rn_drilling.config import CONFIG_29
from department_config.og_30_rn_service.config import CONFIG_30
from department_config.og_5_krasnodarneftgas.config import CONFIG_5
from department_config.og_13_rnpk.config import CONFIG_13
from department_config.og_4_sahalin_mor_neftgas.config import CONFIG_4
from department_config.og_6_severnaya_neft.config import CONFIG_6
from department_config.og_10_uvatneftegaz.config import CONFIG_10
from department_config.og_17_saratovskiy_npz.config import CONFIG_17
from department_config.og_28_rn_trans.config import CONFIG_28
from department_config.og_1_07_08_ank_bashneft.config import CONFIG_1_07_08
# from department_config.ank_bashneft_1_30_07.config import CONFIG_1_30_07
from department_config.og_31_rn_grp.config import CONFIG_31
from department_config.og_24_smolensk_np.config import CONFIG_24
from department_config.og_25_murmansk_np.config import CONFIG_25
from department_config.og_26_vostoknefteproduct.config import CONFIG_26
from department_config.og_101_ank_bashneft.config import CONFIG_101
from department_config.og_102_ank_bashneft.config import CONFIG_102
from department_config.og_103_ank_bashneft.config import CONFIG_103
from department_config.og_104_ank_bashneft.config import CONFIG_104
from department_config.og_105_ank_bashneft.config import CONFIG_105
from department_config.og_106_ank_bashneft.config import CONFIG_106
from department_config.og_107_ank_bashneft.config import CONFIG_107
from department_config.og_108_ank_bashneft.config import CONFIG_108
from department_config.og_12_knpz.config import CONFIG_12
from department_config.og_9_purneftegaz.config import CONFIG_9

env = os.environ

ROOT_DIR = env.get("ROOT_DIR")
RESULT_DIR = env.get("RESULT_DIR")
IS_OVERWRITE = env.get("IS_OVERWRITE", "1") == "1"
IS_WRITE_LOG = env.get("IS_WRITE_LOG", "1") == "1"
DEPARTMENT = env.get("DEPARTMENT")
IS_DELETE_SOURCE = env.get("IS_DELETE_SOURCE")
PART_TO_PARSE = env.get("PART_TO_PARSE", None)

DEPARTMENT_CONFIG = {
    "1_07_08": CONFIG_1_07_08,
    "101": CONFIG_101,
    "102": CONFIG_102,
    "103": CONFIG_103,
    "104": CONFIG_104,
    "105": CONFIG_105,
    "106": CONFIG_106,
    "107": CONFIG_107,
    "108": CONFIG_108,
    "2": CONFIG_2,
    "3": CONFIG_3,
    "4": CONFIG_4,
    "5": CONFIG_5,
    "6": CONFIG_6,
    "7": CONFIG_7,
    "8": CONFIG_8,
    "9": CONFIG_9,
    "10": CONFIG_10,
    "11": CONFIG_11,
    "12": CONFIG_12,
    "13": CONFIG_13,
    "14": CONFIG_14,
    "15": CONFIG_15,
    "16": CONFIG_16,
    "17": CONFIG_17,
    "18": CONFIG_18,
    "19": CONFIG_19,
    "20": CONFIG_20,
    "21": CONFIG_21,
    "22": CONFIG_22,
    "23": CONFIG_23,
    "24": CONFIG_24,
    "25": CONFIG_25,
    "26": CONFIG_26,
    "27": CONFIG_27,
    "28": CONFIG_28,
    "29": CONFIG_29,
    "30": CONFIG_30,
    "31": CONFIG_31,
}

def get_deps(**kwargs):
    """
    Get departments from config
    :param kwargs:
    :return:
    """
    dep_to_parse = [i.strip() for i in kwargs.get("departments").split(',')]
    print(f'dep_to_parse: {dep_to_parse}')
    dep_path_list = {k: v['path'] for k, v in DEPARTMENT_CONFIG.items()}

    department_dict = {k: v for k, v in dep_path_list.items()
            if k in dep_to_parse}

    return {
        'department_to_parse': dep_to_parse,
        'department_dict': department_dict,
    }