import re
from base_parser import Parser as Parser_base
import os
import time
from config import IS_OVERWRITE
import glob
import json
import sys, traceback
from dictionary.column_names import COLUMN_NAMES
import asyncio

def numeric(str_to_check):
    """
    Check if str likes numeric
    :param str_to_check:
    :return:
    """
    return True if str_to_check.isnumeric() else True if re.match('\d+.\d+', str_to_check) is not None else False

class ProcessorRawJson(Parser_base):
    """
    Класс для извлечения данных из jsonlines
    """
    def __init__(self, path, config, path_to_parse, dep_code):
        super().__init__(path, config, path_to_parse, dep_code)

        # Минимальное число ключей в строке, в которой содержится нужная инф-я
        self.minimum_header = config.get('minimum_header', 5)
        # Регулярка для удаления данных, кроме цифр
        self.replace_not_digit = r'\.0*|\[|\]|\s?=\s?|-|\+| - |\s|а|для контроля|б|с$|в|\) общее количество единиц фактически|Кто выдал опись|False|Председатель комиссии|сумма по описи'

        self.match_column = r'\d+'
        self.re_total = re.compile(r'Итого.*', re.IGNORECASE)

        # Список исключений для заголовков
        self.is_not_header = [
            r'Основание для проведения инвентаризаци.*',
            r'Произведено снятие фактических остатков ценностей по состоянию.*на.*2018г',
            r'Наименование учреждения.*Дата*',
            r'По состоянию\s+на.*',
            r'к акту №.*от.*г\.*',
            r'Номер документаДата составленияДата окончания инвентаризации.*',
            r'(ООО \"РН-Краснодарнефтегаз\"){3,}',
            r'Вид операции.*СЛИЧИТЕЛЬНАЯ ВЕДОМОСТЬ',
            r'Произведено снятие фактических остатков ценностей по состоянию на.*',
        ]

        # Список исключений для ОГ
        self.is_not_department = [
            r'Вид деятельности',
            r'наименование Общества',
            r'\(организация\)'
        ]

        # Список пропускаемых ключей в качестве колонок
        self.skip_key_column = [
            r'Филиал.*ПАО',
            r'Лаборатория ООС',
            r'склад\s\d+\s+\(\d+',
        ]

        # Вырианте заголовков, в которых содержится информация "по данным бух. учета"
        self.data_by_account_vars = [
            'data_by_accounting_count', 'accountant_number',
            'reserve_amount_data_by_accounting', 'balance_by_accounting_count',
            'by_data_accounting_count', 'by_data_accounting_count_unt',
            'count_data_by_accounting',
            'cost_data_by_accounting_initial',
            'data_according_accounting_balance_reserve_sum_on_started_inventory',
            'data_by_accounting_cost',
            'data_by_accounting_sum',
            'document_con_by_data_accounting_count_item',
            'count_chestvo',
            'cost_related_accounting_total',
            'cost_data_by_accounting',
            'capitalized_costs_data_by_accounting',
            'actual_availability_cost',
            'data_by_accounting_initial',
            'by_data_count',
            'residue_unworn_catalyst',
            'inventory_items_nominal_code',
            'mass_precious_metals_silver_according_by_records',
            'mass_precious_metals_platinum_according_by_records',
            'capitalized_costs_data_by_accounting',
            'accounted_mass_clean',
            'accounting_count_chestvo',
        ]


        self.exceptions = {}
        # Дополнительные headers, полученные из конфига
        self.additional_header = config.get('header', {})
        # Дополнительные варианты заголовков, полученные из конфига
        self.additional_column_numbers = config.get('column_numbers', {})
        # Спискок масок строк для исключения
        self.exclude_rows = config.get('exclude_rows', {})
        # Спискок масок файлов для исключения
        self.exclude_files = config.get('exclude_files', [])
        self.is_find_header_in_data = config.get('is_find_header_in_data', [])
        self.exclude_data = config.get('exclude_data', [])
        self.many_inventories = config.get('many_inventories', {})
        self.exclude_headers = config.get('exclude_headers', [])
        self.find_header_in_data = config.get('find_header_in_data', [])
        self.exclude_sheet_check_data_by_accounting_count = config.get('exclude_sheet_check_data_by_accounting_count', [])
        self.exclude_sheet_check_header = config.get('exclude_sheet_check_header', [])
        self.check_values = config.get('check_values', {})
        self.skip_data_list = ['TableHeader', 'TableSubHeader', 'simvval', 'SimvValR', 'SimvVal']
        # exclude some files
        if self.exclude_files:
            self.file_list = (f for f in self.file_list
                              if all([re.match(j, f, re.IGNORECASE) is None for j in self.exclude_files]))
        # ---------------------
        self.get_exceptions()
        self.data_list = []
        self.reset()

    def reset(self):
        """
        Сброс к инициализированному состоянию
        :return:
        """
        self.is_write_data = False
        self.mode = self.config.get('mode')
        self.step = 'header' if self.mode == 'register' else 'data_header'
        self.data = {
            'rows': [],
            'header': {},
            'footer': {},
        }

    def get_exceptions(self):
        """
        Сохраняет исключения в self.exceptions
        :return: save exceptions to self.exceptions
        """
        p = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'exceptions')
        files = glob.glob(f'{p}/**/*.json', recursive=True)
        self.exceptions = [
            {
                'full_path': i,
                'part_path': re.sub(p, '', i)
            }
             for i in files
        ]

    async def async_parsing_all(self):
        """
        Запускает процедуру парсинга данных из jsonlines для self.file_list
        :return:
        """
        for i, f in enumerate(self.file_list):
            if self.path_to_parse is not None and self.path_to_parse not in f:
                self.log.info('Skip it for test')
                continue

            _start_time = time.perf_counter()
            self.log.info(f"{i}: Processing file {f}")
            self.log.info(f'start_time: {time.strftime("%H:%M")}')
            await self.async_parsing_one(f)
            self.log.info(f'end_time: {time.strftime("%H:%M")}')
            self.log.info(f"End processing file {f}")
            duration = time.perf_counter() - _start_time
            self.log.info(f"duration: {duration:0.2f} seconds.")
            self.log.info(f"-----------------------")
            self.reset()

    async def async_parsing_one(self, f):
        """
        Запускает процедуру парсинга данных из jsonlines для одного файла async
        :return:
        """
        file_size = round(os.path.getsize(f) / (1024 * 1024), 2)
        self.log.info(f'file_size: {file_size} Mb')
        if file_size > 2:
            await asyncio.sleep(0.00001)
        self.parsing_one(f)

    def parsing_one(self, path):
        """
        Запускает процедуру парсинга данных из jsonlines для одного файла
        :return:
        """
        self.log.info(f"\treading file...")
        file_list = glob.glob(path)
        for f in file_list:
            if os.path.getsize(f) == 0:
                self.log.info(f'Skip empty file {f}')
                continue

            self.get_name_to_save(f)

            if os.path.isfile(self.path_to_save) and not IS_OVERWRITE:
                self.log.info(f'Skip file {f}')
                continue

            _exceptions = [i['full_path'] for i in self.exceptions if i['part_path'] in f]

            if _exceptions:
                self.log.info(f'Use file from exceptions: {_exceptions[0]}')
                _f = _exceptions[0]
            else:
                _f = f

            res = self.get_info_from_sheet_json(_f)
            if res != 'Not_found':
                if self.data_list:
                    for num,i in enumerate(self.data_list):
                        _path_splited = _f.split('_json/')
                        dep_structured = i['header'].get('department_structured').replace('/', '_')
                        _path = f"{_path_splited[0]}__{num}/{dep_structured}/{_path_splited[1]}"
                        self.data = i
                        self.data['src'] = path
                        try:
                            self.get_name_to_save(_path)
                            self.log.info('Save Success!')
                        except OSError as e:
                            try:
                                dep_structured = f'{dep_structured[:15]}**{dep_structured[-15:]}'
                                _path = f"{_path_splited[0]}__{num}/{dep_structured}/{_path_splited[1]}"
                                self.get_name_to_save(_path)
                                self.log.info('Save Success!')
                            except Exception as e:
                                traceback.print_exc()
                                self.log.error(e)
                        self.save()
                else:
                    self.data['src'] = path
                    self.save()


    def is_column_numbers(self, row_list, custom_column_number={}):
        """
        Проверка, является ли строка нумерацие колонок (пример 1|2|3|4)
        :param row_list:
        :param custom_column_number:
        :return: True if row is numeric order else False
        """
        try:
            if len(row_list) < self.minimum_header:
                return False

            # hash_row = ''.join([re.sub(r'\.0*', '', i) for i in row_list])
            val_reg = [re.sub(self.replace_not_digit, '', j, re.IGNORECASE).replace('False', '0') for j in row_list]
            is_all_digit = all([j.isdigit() and len(j) < 5 for j in val_reg if len(j) > 0])
            if is_all_digit:
                return True

            if custom_column_number:
                row_str = ''.join([j for j in row_list])
                if re.match(custom_column_number, row_str):
                    return True

        except Exception as e:
            traceback.print_exc()
            self.log.error(e)

        return False

    def is_total_row(self, row_list):
        """
        Проверка, содержит ли строка инфо об итоговых данных
        :param row_list:
        :return: True if row contains info about total count rows
        """
        total_find = list(filter(self.re_total.match, row_list))
        if total_find:
            return True
        return False

    def get_rows_header(self, row_list):
        """
        Возвращает заголовки из таблицы с описями
        :param row_list:
        :return: headers for register
        """
        skip_data = [True for i in self.exclude_data if re.match(i, self.path_to_save, re.IGNORECASE)]
        if skip_data:
            return {}

        data_header = {}
        h_key = None
        for rownum, i in enumerate(row_list):
            is_all_digit = all([re.sub(r'\.0*|\[|\]', '', j, re.IGNORECASE).isdigit() for j in i.values()])
            if is_all_digit:
                continue

            for k, val_d in i.items():
                val = val_d['value'] if 'value' in val_d else val_d
                is_found = False
                v = re.sub(r'\n|\d$', '', val).strip()
                col = re.sub(r'\d', '', k)

                if len(v) == 0:
                    continue

                if rownum > 0 and self.mode != 'form':
                    if col in data_header:
                        h_key = data_header[col]

                    for k_col, v_col in COLUMN_NAMES['data'].items():
                        try:
                            if re.match(v_col['match'], v, flags=re.IGNORECASE):
                                if h_key is None:
                                    code = k_col
                                else:
                                    code = f'{h_key}_{k_col}'

                                data_header[col] = code
                                is_found = True
                                break
                        except Exception as e:
                            self.log.error(e)
                else:
                    for k_col, v_col in COLUMN_NAMES['data'].items():
                        try:
                            if re.match(v_col['match'], v, flags=re.IGNORECASE):
                                data_header[col] = k_col
                                is_found = True
                                break
                        except Exception as e:
                            self.log.error(e)

                if not is_found:
                    is_exclude_from_not_found = any([True for k in self.skip_key_column if re.match(k, v, re.IGNORECASE)])
                    if not is_exclude_from_not_found:
                        self.log.error(f'Not found key: {v}')
                        self.log.error(f'file: {self.path_to_save}')

        return data_header

    def get_row_str(self, row_dict, delimiter=''):
        """
        Возращает jsonline как строку с данными
        :param row_dict:
        :param delimiter:
        :return: jsonline as str
        """
        row_str = None
        try:
            row_str = delimiter.join(
                [str(v) for k, v in sorted(row_dict.items(), key=lambda x: self.col_to_num(x[0]))]
            ).replace('\n', '')
        except Exception as e:
            traceback.print_exc()
            self.log.error(e)

        return row_str

    def get_data(self, data_header, row_list):
        """
        Возращает данные по описям из таблицы
        :param data_header:
        :param row_list:
        :return: data for register list
        """
        data = []
        for i in row_list:
            is_all_digit = None
            try:
                is_all_digit = all([re.sub(r'\.0*|\[|\]', '', j, re.IGNORECASE).isdigit() for j in i.values()])
            except Exception as e:
                traceback.print_exc()
                self.log.error(e)

            if is_all_digit:
                continue

            row_value = {}

            if row_value:
                data.append(row_value)
        # ---------------
        is_find_header_in_data = any([True for i in self.is_find_header_in_data if re.match(i, self.path_to_save, re.IGNORECASE)])
        if is_find_header_in_data:
            self.get_header(row_list)

        return data

    def col_to_num(self, col_str):
        """ Convert base26 column string to number. """
        expn = 0
        col_num = 0
        for char in reversed(col_str):
            col_num += (ord(char) - ord('A') + 1) * (26 ** expn)
            expn += 1

        return col_num

    def check_organization(self, s):
        """
        Проверка, содержит ли строка "Организацию"
        :param s:
        :return: True if str like oragnization
        """
        if s is None:
            return False
        cl_data = re.sub(r'\)|Номер.*', '', s, re.IGNORECASE).strip()
        if len(cl_data) == 0:
            return False
        return True

    def check_dep_structured(self, s):
        """
        Проверка, содержит ли строка "Подразделение"
        :param s:
        :return: True if str like department
        """
        if s is None:
            return False
        return True

    def get_header(self, row_list, custom_headers={}):
        """
        Получает данные из "шапки"
        :param row_list:
        :param custom_headers:
        :return: save header to self
        """
        header = self.data['header']

        column_headers = COLUMN_NAMES['header'].copy()
        column_headers.update(custom_headers)

        headers_in_row = {k: v for k, v in column_headers.items() if v.get('place') == 'in_row'}
        headers_prev_row = {
            k: v for k, v in column_headers.items() if k in ['organization_prev_row', 'department_structured_prev_row']
        }
        headers_in_filename = {k: v for k, v in column_headers.items() if v.get('place') == 'in_filename'}

        buffer = []

        for num, i in enumerate(row_list):
            is_found = False
            # find in header

            row_str = self.get_row_str(i, delimiter=' ')
            buffer.append(row_str)

            try:
                # find in row
                for k, v in headers_in_row.items():
                    if header.get(k) is None:
                        try:
                            _match = re.search(v['match'], row_str, re.IGNORECASE)
                        except Exception as e:
                            traceback.print_exc()
                            self.log.error(e)
                            _match = None

                        if _match:
                            try:
                                _vals = [i for i in _match.groups() if i]
                                _val = _vals[0] if _vals else None
                            except IndexError:
                                continue

                            if k == 'document_number' and _val and len(re.sub('\D', '', _val)) == 0 and not _val.startswith('ОС-'):
                                continue

                            if k == 'organization' and re.match(r'^организация\)', _val, re.IGNORECASE):
                                continue

                            if _val and len(_val) > 0:
                                if k == 'document_number':
                                    _val = re.sub(r'\.0+$', '', str(_val))

                                header[k] = _val
                                is_found = True

                if is_found:
                    continue

            except Exception as e:
                traceback.print_exc()
                self.log.error(e)
            # -----------------
        # find organization and department structed in prev row
        if buffer and not (self.check_organization(header.get('organization'))
                       and self.check_dep_structured(header.get('department_structured'))):

            for j in range(1, len(buffer)):
                for k, v in headers_prev_row.items():
                    _row = buffer[j]
                    _match = re.search(v['match'], _row, re.IGNORECASE)

                    if _match and header.get(k.replace('_prev_row', '')) is None:
                        prev_row = buffer[j - 1]
                        _k = k.replace('_prev_row', '')
                        prev_row = re.sub(v['remove_pattern'], '', prev_row, re.IGNORECASE)
                        header[_k] = prev_row
                        break

        header = {k: v for k, v in header.items() if len(v) > 0}

        # find header for filename
        for k_col, v_col in headers_in_filename.items():
            _match = re.match(v_col['match'], self.path_to_save, flags=re.IGNORECASE)
            if _match:
                header[k_col] = _match.group(1)

        self.data['header'] = header

    def is_possible_header(self, row_list):
        possible_headers = self.config.get('register').get('possible_header')
        if possible_headers is None:
            return False, None

        try:
            possible_header_keys = [
                v for k, v in possible_headers.items()
                if re.match(k, self.path_to_save, re.IGNORECASE)
            ]

            possible_header = {}

            for p in possible_header_keys:
                possible_header.update(p)

            if possible_header:
                row_str = ''.join([str(v) for k, v in sorted(row_list.items(), key=lambda x: self.col_to_num(x[0]))]).replace('\n', '')

                last_header_row = possible_header.get('last_header_row')
                if last_header_row and re.match(last_header_row, row_str):
                    return True, 'last_header_row'

                first_header_row = possible_header.get('first_header_row')
                if first_header_row and re.match(first_header_row, row_str):
                    return False, 'first_header_row'

        except Exception as e:
            traceback.print_exc()
            self.log.error(e)

        return False, None

    def is_header_row_start(self, row_list):
        """
        Проверка, явлется ли строка первой строкой заголовка для данных
        :param row_list:
        :return: True if row is start for data
        """
        try:
            hash_row = ''.join([re.sub(r'\.0*', '', i) for i in row_list])
            if len(row_list) >= self.minimum_header:
                for i in self.is_not_header:
                    if re.match(i, hash_row, re.IGNORECASE):
                        return False
                return True
            return False
        except Exception as e:
            self.log.error(e)

        return False

    def get_data_from_footer(self, row):
        """
        Сохраняет данные из футера
        :param row:
        :return: save data from footer
        """
        for k, v in COLUMN_NAMES['footer'].items():
            _match = re.search(v['match'], row, re.IGNORECASE)
            if _match:
                if self.data['footer'].get(k) is None:
                    self.data['footer'][k] = _match.group(1)
                else:
                    self.data['footer'][k] = f"{self.data['footer'][k] }__{_match.group(1)}"

    def remove_excluded_rows(self, row_list, exclude_row={}):
        """
        Фильтрует строки, переменная del_reason служит для отладки и показывает причину удаления строки
        :param row_list:
        :param exclude_row: config to exclude row
        :return:
        """
        if exclude_row:
            rows = (i for i in row_list)
            new_data = row_list[:]
            try:
                required = exclude_row.get('required', [])
                number_digit = exclude_row.get('number_digit')
                is_check_numbers = exclude_row.get('is_check_numbers', False)
                minimum_header = exclude_row.get('minimum_data_keys',
                                                 exclude_row.get('minimum_header', self.minimum_header))

                self.log.info(f"Len data before cleaning: {len(row_list)}")
                is_del = False
                del_reason = None

                for num, i in enumerate(rows):
                    is_del = False
                    if len(i.keys()) < minimum_header or any([re.match('Итого.*', j) for j in i.values()]):
                        is_del = True
                        del_reason = 'len_result'

                    if not is_del:
                        if is_check_numbers:
                            is_all_digit = all([re.sub(r'\.0*|\[|\]|а|\.', '', j, re.IGNORECASE).isdigit() for j in i.values()])
                            if is_all_digit:
                                is_del = True
                                del_reason = 'is_all_digit'

                    if not is_del:
                        for r in required:
                            is_not_exists_number = all([re.match(r, j, re.IGNORECASE) is None for j in i])
                            if is_not_exists_number:
                                is_del = True
                                del_reason = 'required'
                                break

                    if not is_del and number_digit is not None:
                        digit_keys = [j for j in i if re.match(number_digit, j, re.IGNORECASE)]
                        if digit_keys:
                            digit_key = digit_keys[0]
                            if not digit_key or i.get(digit_key) is None:
                                is_del = True
                                del_reason = 'number_digit'

                            if not is_del:
                                _number = re.sub(r'\.|0|/', '', i.get(digit_key))
                                if not _number.isdigit():
                                    is_del = True
                                    del_reason = 'number_digit_1'

                    if is_del:
                        new_data.remove(i)

                self.log.info(f"Len data after cleaning: {len(new_data)}")
                return new_data

            except Exception as e:
                traceback.print_exc()
                self.log.error(e)
        else:
            return row_list

    def get_split_data(self, row_list):
        """
        Разделяет данные, если в одном листе хранятся данные по нескольким описям
        :param row_list:
        :return:
        """
        if self.more_inventory:
            row_str = self.get_row_str(row_list)
            if self.data.get('header') or self.data.get('data'):
                if re.match(self.more_inventory['split_row'], row_str, re.IGNORECASE):
                    self.data_list.append(self.data)
                    self.reset()
                    return True

    def get_info_from_sheet_json(self, path):
        """
        Получает инфо из одного листа
        :param path:
        :return:
        """
        custom_headers = [
            v for k, v in self.additional_header.items()
            if re.match(k, path, re.IGNORECASE)
        ]

        custom_column_numbers = [
            v for k, v in self.additional_column_numbers.items()
            if re.match(k, path, re.IGNORECASE)
        ]

        exclude_rows = [
            v for k, v in self.exclude_rows.items()
            if re.match(k, path, re.IGNORECASE)
        ]

        many_inventories_list = [
            v for k, v in self.many_inventories.items()
            if re.match(k, path, re.IGNORECASE)
        ]

        self.more_inventory = {}
        for mil in many_inventories_list:
            self.more_inventory.update(mil)

        self.exclude_row = {}
        for ex_row in exclude_rows:
            self.exclude_row.update(ex_row)

        # update minimum header
        if self.exclude_row:
            self.minimum_header = self.exclude_row.get('minimum_header', self.minimum_header)

        self.custom_column_number = {}
        if custom_column_numbers:
            self.custom_column_number = custom_column_numbers[0]

        self.custom_header = {}
        if custom_headers:
            self.custom_header = custom_headers[0]

        is_find_header_in_data = any([
            re.match(i, self.path_to_save, re.IGNORECASE) is not None
            for i in self.find_header_in_data
        ])

        try:
            num = 0
            self.is_write_data = False
            data_header = {}

            row_buffer = []

            is_column_numbers_were = False
            is_possible_header_was = False
            data_header_rownum = None
            is_not_write_if_last = False
            data_header_rownum_end = sys.maxsize

            line = None
            is_last_row = is_column_numbers = is_total = is_possible_header = False

            with open(path) as f:
                try:
                    while True:
                        if line is not None and len(line) > 0:
                            # get row data
                            if self.step == 'rows':
                                row_str = ' '.join([j for j in i.values()])
                                self.get_data_from_footer(row_str)

                                if self.is_write_data or is_last_row:
                                    is_next = True

                                    if is_last_row and is_not_write_if_last:
                                        is_next = False

                                    if is_next:
                                        data = self.get_data(data_header, row_buffer)
                                        self.data['rows'].extend(data)
                                        self.is_write_data = False
                                        is_column_numbers_were = False
                                        if is_find_header_in_data:
                                            self.get_header(row_buffer)

                                        row_buffer = []
                                        if is_possible_header_was:
                                            is_not_write_if_last = True
                                            is_possible_header_was = False

                            if is_total and (is_column_numbers_were or is_possible_header_was):
                                self.is_write_data = True
                            elif is_column_numbers:
                                is_column_numbers_were = True
                                self.is_write_data = False
                            elif is_possible_header:
                                is_possible_header_was = True
                                self.is_write_data = False

                        if is_last_row:
                            break
                        # -----------------------
                        line = f.readline()
                        if len(line) == 0:
                            continue

                        is_last_row = 'END_FILE' in line
                        if is_last_row and not self.data.get('header'):
                            self.get_header(row_buffer, self.custom_header)

                        if not is_last_row:
                            try:
                                r = json.loads(line)
                            except Exception as e:
                                traceback.print_exc()
                                self.log.error(e)

                            try:
                                i = {k: str(v).strip() for k, v in r.items()
                                     if len(str(v)) > 0 and v not in self.skip_data_list}
                            except Exception as e:
                                traceback.print_exc()
                                self.log.error(e)

                            if i:
                                row_buffer.append(i)
                                num += 1
                                if num % 1000 == 0:
                                    print('.', end='')

                                values_list = [i[key] for key in sorted(i.keys())]
                                is_split_data = self.get_split_data(r)
                                if is_split_data:
                                    data_header_rownum = None
                                    num = 0
                                    row_buffer = []

                                is_column_numbers = self.is_column_numbers(values_list, self.custom_column_number)
                                is_total = self.is_total_row(values_list)
                                is_possible_header, is_possible_header_flag = self.is_possible_header(r)

                                if (self.is_header_row_start(values_list) or is_possible_header_flag == 'first_header_row')\
                                        and data_header_rownum is None:
                                    data_header_rownum = num -1

                                if is_column_numbers:
                                    data_header_rownum_end = num - 1

                                # get header
                                if ((is_column_numbers or is_possible_header) and self.step == 'header') \
                                        or (is_last_row and not self.data['header'] and not self.data['rows']):
                                    self.step = 'data_header'

                                    self.get_header(row_buffer, self.custom_header)
                                    if is_possible_header_flag == 'last_header_row':
                                        data_header_rownum_end = num - 1

                                # get headers of data
                                if self.step == 'data_header' and (
                                        is_column_numbers or is_possible_header or is_possible_header_was):

                                    is_next = True
                                    if is_possible_header and not is_possible_header_flag == 'last_header_row':
                                        is_next = False
                                        if not is_possible_header_was:
                                            row_buffer = []
                                        is_possible_header_was = True

                                    if is_possible_header_was and not is_possible_header:
                                        is_next = True

                                    if is_next:
                                        self.step = 'rows'
                                        # find headers
                                        if is_possible_header_was:
                                            _rows_header = row_buffer[:]
                                        else:
                                            if self.mode == 'form':
                                                _rows_header = row_buffer[-2:-1]
                                            else:
                                                if is_possible_header_flag == 'last_header_row':
                                                    data_header_rownum_end = num + 1

                                                _rows_header = row_buffer[data_header_rownum:data_header_rownum_end][:]

                                        data_header = self.get_rows_header(_rows_header)
                                        row_buffer = []

                except Exception as e:
                    traceback.print_exc()
                    self.log.error(e)

            if num == 0:
                self.log.info("Data not found")
                self.log.info(f'Skip file {path}')
                return 'Not_found'

        except Exception as e:
            traceback.print_exc()
            self.log.error(e)

        # -----------------------------
        if self.more_inventory:
            data_list = (i for i in self.data_list)
            for _num, i in enumerate(data_list):
                i['rows'] = self.remove_excluded_rows(i['rows'], self.exclude_row)
                self.data_list[_num] = i

        else:
            self.data['rows'] = self.remove_excluded_rows(self.data['rows'], self.exclude_row)
        # ----------------
        try:
            is_skip = any([
                re.match(i, self.path_to_save, re.IGNORECASE) is not None
                for i in self.exclude_sheet_check_data_by_accounting_count
            ])

            _rows = self.data['rows']
            if re.match(r'.*/НКС/.*', self.path_to_save, re.IGNORECASE):
               if _rows:
                   for _r in _rows:
                       _r['data_by_accounting_count'] = 1

            elif self.mode == 'register':

                if not is_skip and _rows is not None:
                    all_data_by_accounting_count = []
                    for _acc_count in self.data_by_account_vars:
                        all_data_by_accounting_count = [
                            j.get(_acc_count) for j in _rows
                            if j.get(_acc_count) is not None and numeric(j.get(_acc_count))
                        ]

                        if all_data_by_accounting_count:
                            break

                    if not all_data_by_accounting_count:
                        self.log.warning(f'file: {self.path_to_save}')
                        self.log.warning('all_data_by_accounting_count is empty!')

        except Exception as e:
            is_skip = False
            self.log.error("Error during getting all all_data_by_accounting_count")
            traceback.print_exc()
            self.log.error(e)

        is_skip_header = any([
            re.match(i, self.path_to_save, re.IGNORECASE) is not None
            for i in self.exclude_sheet_check_header
        ])

        if self.mode != 'form' and not is_skip_header\
                and self.data.get('header') and self.data['header'].get('document_number') is None:
            self.log.warning(f'file: {self.path_to_save}')
            self.log.warning(f'document_number is not exists!')
        elif is_skip_header and self.data.get('header') and self.data['header'].get('document_number'):
            self.data['header'].pop('document_number')
        
        for check_value, check_path in self.check_values.items():
            is_check = re.match(check_path, self.path_to_save, re.IGNORECASE) # is not None
            if is_check and self.data.get('header') and self.data['header'].get(check_value) is None:
                self.log.warning(f'file: {self.path_to_save}')
                self.log.warning(f'{check_value} is not in header!')
            elif check_value == 'department_structured' and is_check\
                and self.data.get('header') and self.data['header'].get(check_value):
                if self.data['header'].get(check_value) in self.is_not_department:
                    self.log.warning(f'file: {self.path_to_save}')
                    self.log.warning(f'{check_value} is not in header!')
                    self.data['header'].pop(check_value)

        self.log.info(f'header: {self.data["header"]}')
        self.log.info(f'Count rows {len(self.data["rows"])}')