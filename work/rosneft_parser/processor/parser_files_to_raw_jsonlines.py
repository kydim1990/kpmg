import re
from base_parser import Parser as Parser_base
import os
import time
from config import ROOT_DIR, RESULT_DIR, IS_OVERWRITE, PART_TO_PARSE
import json
from utils.utils import get_data_from_xls
import sys, traceback
from docx_to_json_lines import DocxParser


class Parser(Parser_base):
    """
    Класс извлечения данных из эксель в jsonlines
    """
    empty_row_pattern = r"=NOT\(ISERROR\(\(MATCH\(.*\)\)\)'\}$"

    async def async_parsing_one(self, f):
        """
        Запускает процедуру конвертирование эксель в jsonlines для одного файла async
        :param f:
        :return:
        """
        self.parsing_one(f)

    async def async_parsing_all(self):
        """
        Запускает процедуру конвертирование эксель в jsonlines для self.file_list
        :return:
        """
        for i, f in enumerate(self.file_list):
            if PART_TO_PARSE is not None and PART_TO_PARSE not in f:
                print('Skip it for test')
                continue

            self.log.info(f"{i}: Processing file {f}")
            _start_time = time.perf_counter()
            self.log.info(f'start_time: {time.strftime("%H:%M")}')
            await self.async_parsing_one(f)
            self.log.info(f'end_time: {time.strftime("%H:%M")}')
            self.log.info(f"End processing file {f}")
            duration = time.perf_counter() - _start_time
            self.log.info(f"duration: {duration:0.2f} seconds.")
            self.log.info(f"-----------------------")

    def parser_old_xls(self, path):
        """
        Парсинг для старых форматов xls
        :param path:
        :return:
        """
        book_sheets = get_data_from_xls(path, only_sheet_name=True)
        if book_sheets is None:
            return

        if not IS_OVERWRITE:
            all_exist = True
            try:
                for i in book_sheets:
                    self.get_name_to_save(path, i)
                    if not os.path.isfile(self.path_to_save):
                        all_exist = False
                        break

                if all_exist:
                    self.log.info(f'Skip, all files exist from file {path}')
                    return
            except Exception as e:
                traceback.print_exc()
                self.log.error(e)

        book = get_data_from_xls(path)
        if book == 'Error':
            self.log.error('Error reading')
            return

        try:
            for k, v in book.items():
                self.get_name_to_save(path, k)
                if os.path.isfile(self.path_to_save) and not IS_OVERWRITE:
                    self.log.info(f'Skip file {self.path_to_save}')
                    continue

                with open(self.path_to_save, "a+") as f:
                    for i in v:
                        row = json.dumps(i, ensure_ascii=False)
                        f.write(f'{row}\n')

                    f.write(f'END_FILE')

        except Exception as e:
            traceback.print_exc()
            self.log.error(e)

    def parsing_one(self, path):
        """
            Запускает процедуру конвертирование эксель в jsonlines для одного файла
            :param f:
            :return:
        """
        file_size = round(os.path.getsize(path) / (1024 * 1024), 2)
        self.log.info(f'file_size: {file_size} Mb')

        self.log.info(f"\treading file...")
        self.data['src'] = path

        self.expansion = path.split('.')[-1]
        if self.expansion == 'docx':
            print('Parsing docx by DocxParser')
            parser = DocxParser(path)
            parser.parse()
            print('Success!')
            return

        if self.expansion == 'xls':
            self.parser_old_xls(path)
            return

        book = self.get_book(path)
        if book is None:
            self.parser_old_xls(path)
            return

        try:
            for sheet in book.worksheets:
                self.get_name_to_save(path, sheet.title)

                if os.path.isfile(self.path_to_save) and not IS_OVERWRITE:
                    self.log.info(f'Skip file {self.path_to_save}')
                    continue

                with open(self.path_to_save, "a+") as f:
                    for i in self.sheet_to_jsonline(sheet):
                        row = json.dumps(i, ensure_ascii=False)
                        f.write(f'{row}\n')

                    f.write(f'END_FILE')

                self.log.info(f'Saved file {self.path_to_save} !')

        except Exception as e:
            traceback.print_exc()
            self.log.error(e)
            return 'error'
        finally:
            book.close()

    def get_name_to_save(self, path, sheet_title):
        """
        Возвращает имя файла, в который будет сохранен результат
        :param path:
        :param sheet_title:
        :return:
        """
        dir_to_save = os.path.dirname(path).replace(ROOT_DIR, RESULT_DIR)

        new_path = os.path.join(dir_to_save, os.path.basename(path))
        pre, ext = os.path.splitext(new_path)
        p = f'{pre}_json'
        if not os.path.exists(p):
            os.makedirs(p)

        path_to_save = os.path.join(f'{pre}_json', f'{sheet_title}.json')

        self.path_to_save = path_to_save

    def sheet_to_jsonline(self, sheet):
        """
        Преобразует sheet to jsonlines
        :param sheet:
        :return:
        """
        def is_skip_row(row_check):
            if len(row_check.keys()) > 1:
                return False

            if row_check:
                return False

            row_str = ''.join([i for i in row_check.values()])
            if re.match(self.empty_row_pattern, row_str):
                return True

            return False

        try:
            for num, row in enumerate(sheet.rows):
                if num % 50 == 0:
                    print(f'.', end='.')

                row_json = {}
                cells = (i for i in row if i.value is not None)
                for cell in cells:
                    letter = self.get_letter(cell)
                    cell_value = self.get_cell_value(cell)

                    code = f'{letter}{cell.row}'
                    row_json[code] = cell_value

                if row_json and not is_skip_row(row_json):
                    yield row_json
        except Exception as e:
            traceback.print_exc()
            self.log.error(e)
