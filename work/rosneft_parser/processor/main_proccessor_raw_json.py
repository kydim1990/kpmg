from config import DEPARTMENT_CONFIG, get_deps
from config import ROOT_DIR
import os
from processor.processor_raw_json import ProcessorRawJson
import time
import asyncio
import argparse

async def async_main(deps):
    """
        Запускает процедуру извлечения данных из jsonlines
        :return:
    """
    print(f'ROOT_DIR: {ROOT_DIR}')

    s = time.perf_counter()
    for i in deps['department_to_parse']:
        print(f'Parse department: {i}')
        config_department = DEPARTMENT_CONFIG[i]
        print(f"config_department form glob: {config_department['form']['glob']}")

        # parse forms
        for glob in config_department['form']['glob']:
            path = os.path.join(ROOT_DIR, config_department["path"], glob)
            print(f'path: {path}')
            config_department['mode'] = 'form'
            parser = ProcessorRawJson(path, config=config_department, path_to_parse=deps['path_to_parse'], dep_code=i)
            parser.parsing_all()

        print(f"config_department register glob: {config_department['register']['glob']}")
        # parsing register
        for glob in config_department['register']['glob']:
            path = os.path.join(ROOT_DIR, config_department["path"], glob)
            print(f'path: {path}')
            config_department['mode'] = 'register'
            parser = ProcessorRawJson(path, config=config_department, path_to_parse=deps['path_to_parse'], dep_code=i)
            await parser.async_parsing_all()

        print(f'End parse department: {i}')
    duration = time.perf_counter() - s
    print(f"duration: {duration:0.2f} seconds.")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Get data from json lines')
    parser.add_argument('-dep', '--departments', type=str)
    parser.add_argument('-ptp', '--path_to_parse', type=str, default='/')

    args = parser.parse_args()

    deps = get_deps(**{
        'departments': args.departments,
    })

    deps['path_to_parse'] = args.path_to_parse

    asyncio.run(async_main(deps))
