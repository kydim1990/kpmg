from config import ROOT_DIR, get_deps
import os
from processor.parser_files_to_raw_jsonlines import Parser
import asyncio
import time
import argparse

async def async_main(deps):
    s = time.perf_counter()
    """
        Для каждого ОГ запускаем извлечения данных из эксель в jsonlines
    """
    for v in deps.values():
        for i in ['*[xX][lL][sS]*', '*.doc*']:
            path = os.path.join(ROOT_DIR, v, '**', i)
            print(f'path={path}')
            parser = Parser(path=path, config={})
            await parser.async_parsing_all()

    duration = time.perf_counter() - s
    print(f"duration: {duration:0.2f} seconds.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Excel to json lines')
    parser.add_argument('-dep', '--departments', type=str)
    parser.add_argument('-ptp', '--path_to_parse', type=str)

    args = parser.parse_args()

    deps = get_deps(**{
        'departments': args.departments,
    })
    # main()
    asyncio.run(async_main(deps['department_dict']))