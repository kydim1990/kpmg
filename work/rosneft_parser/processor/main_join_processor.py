from config import DEPARTMENT_CONFIG, get_deps
from config import ROOT_DIR
import os
from processor.processor_join_data import ProcessorJoinJson
import argparse

def main():
    """
    Запускает процедуру слияние файлов из папки <name>__json
    :return:
    """
    for i in deps['department_dict']:
        print(f'Parse department: {i}')
        config_department = DEPARTMENT_CONFIG[i]

        for glob in config_department['join_json']['glob']:
            path = os.path.join(ROOT_DIR, config_department["path"], glob)
            join_conf = config_department.get("join_config", {})
            parser = ProcessorJoinJson(path, path_to_parse=deps['path_to_parse'], config=join_conf)
            parser.parsing_all()

        print(f'End parse department: {i}')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Json to one json')
    parser.add_argument('-dep', '--departments', type=str)
    parser.add_argument('-ptp', '--path_to_parse', type=str)

    args = parser.parse_args()

    deps = get_deps(**{
        'departments': args.departments,
    })

    deps['path_to_parse'] = args.path_to_parse

    main()