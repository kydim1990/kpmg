from utils.utils import delete_folder_by_mask
import os
import argparse

ROOT = os.environ.get('ROOT_DIR')
MASK = "/{}*/**/*_json"

if __name__ == '__main__':
    """
    Remove files by mask
    """
    parser = argparse.ArgumentParser(description='remove dir by mask')
    parser.add_argument('-dep', '--departments', type=str)
    args = parser.parse_args()

    dep_mask = MASK.format(args.departments)
    mask = f'{ROOT}{dep_mask}'
    input(f"mask: {mask}, Are you ready?")
    delete_folder_by_mask(mask)