import re
from base_parser import Parser as Parser_base
import os
import glob
import json
import time
from config import IS_OVERWRITE

class ProcessorJoinJson(Parser_base):
    """
       Класс слияния данных из папки <filename>__json
   """
    def __init__(self, path, config, path_to_parse=None):
        super().__init__(path, config, path_to_parse)
        self.is_separated = False

    def parsing_all(self):
        """
        Запускает процедуру слияния данных для self.file_list
        :return:
        """
        is_separated_list = self.config.get('is_separated')
        for i, f in enumerate(self.file_list):
            if self.path_to_parse is not None and self.path_to_parse not in f:
                self.log.info('Skip it for test')
                continue

            self.is_separated = False
            if is_separated_list:
                is_separated = [i for i in is_separated_list if re.match(i, f, re.IGNORECASE)]
                self.is_separated = is_separated != []

            self.get_name_to_save(f)
            if os.path.isfile(self.path_to_save) and not IS_OVERWRITE:
                self.log.info(f'{i} Skip file {f}')
                continue

            self.log.info(f"{i}: Processing file {f}")
            self.log.info(f'start_time: {time.strftime("%H:%M")}')
            parse_result = self.parsing_one(f)
            if parse_result == "error":
                self.log.error("Error during parsing")
                continue

            if not self.is_separated:
                self.save()
            self.log.info(f'end_time: {time.strftime("%H:%M")}')
            self.log.info(f"End processing file {f}")
            self.log.info(f"-----------------------")

    def parsing_one(self, path):
        """
        Запускает процедуру слияния данных для одного файла
        :param f:
        :return:
        """
        self.log.info(f"reading file...")

        file_list = glob.glob(f'{path}/*.json')
        for f in file_list:
            data = self.get_info_from_file(f)

            if data is None:
                self.log.info(f'Skip, data is empty')
                continue

            if self.is_separated:
                sheet_name = os.path.basename(f).replace('.json', '').rstrip('/')
                for k, v in data.items():
                    self.data[k] = v
                self.data['src'] = re.sub(r'_json$', f'.xls_{sheet_name}', path)
                p = self.path_to_save.split('.json')
                prev_save = self.path_to_save
                self.path_to_save = f'{p[0]}_{sheet_name}.json'
                self.save()
                self.path_to_save = prev_save
            else:
                for k, v in data.items():
                    if k == 'rows':
                        self.data[k].extend(x for x in v if x not in self.data[k])
                    elif k == 'header':
                        # is_key_exists = [i for i in v if i in self.data[k]]
                        self.data[k].update(v)

        self.data['src'] = re.sub(r'_json$', '.xls_', path)

    def get_info_from_file(self, path):
        """
        Извлекает информацию из файла json
        :param path:
        :return:
        """
        try:
            with open(path) as f:
                data = json.load(f)
            return data

        except Exception as e:
            self.log.error(e)


    def get_name_to_save(self, path):
        """
        Возвращает имя файла, в который будет сохранен результат
        :param path:
        :param sheet_title:
        :return:
        """
        path_to_save = re.sub(r'_json$', '.json', path)
        if not os.path.exists(os.path.dirname(path_to_save)):
            os.makedirs(os.path.dirname(path_to_save))

        self.path_to_save = path_to_save

    def save(self):
        """
        Сохраняет результат в файл
        :return: save result to file
        """
        if len(self.data['rows']) == 0:
            self.log.info("!! rows_is_empty !!")

        self.log.info(f"Saving ...")

        self.log.info(f"Save result to file {self.path_to_save}")

        try:
            with open(self.path_to_save, "w") as f:
                json.dump(self.data, f, ensure_ascii=False, indent=4)
        except Exception as e:
            self.log.error(e)

        self.data = {
            'rows': [],
            'header': {},
            'footer': {}
        }
        self.log.info(f"End saving result to file {self.path_to_save}")