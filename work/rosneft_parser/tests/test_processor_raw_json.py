import pytest
import os
from config import get_deps
from processor.main_proccessor_raw_json import async_main
import asyncio
import shutil
import glob
import json


RESULT_DIR = os.environ.get('RESULT_DIR')
ROOT_DIR = os.environ.get('ROOT_DIR')
deps_str = os.environ.get('DEPS_STR')

def setup():
    """
    Clear output directory, start converting excel to jsonlines,
     save paths in variables
    :return: None
    """
    shutil.rmtree(RESULT_DIR, ignore_errors=True)
    deps = get_deps(**{
        'departments': deps_str,
    })
    deps['path_to_parse'] = '/'
    asyncio.run(async_main(deps))
    read_files()

def read_files():
    """
    Create pair (output, expected) for comparing
    :return:
    """
    output = glob.glob(f"{RESULT_DIR}/**/*.json", recursive=True)
    expected = [i.replace("/output/", "/expected/") for i in output]
    return [(output[i], expected[i]) for i in range(len(expected))]

setup()
test_list = read_files()

class TestProcessRaw:

    @pytest.mark.parametrize("data, expected", test_list)
    def test_process(self, data, expected):
        """
        Compare expected and output, exclude src
        :param data:
        :param expected:
        :return:
        """
        with open(data) as f:
            data_json = json.load(f)

        with open(expected) as f:
            expected_json = json.load(f)

        data_json.pop('src')
        expected_json.pop('src')
        assert data_json == expected_json

