#!/usr/bin/env bash
export PYTHONPATH=.
#export ROOT_DIR=/home/d/data/rosneft
export ROOT_DIR="/data/projects/Rosneft/Prepared data/"
#export RESULT_DIR=/home/d/data/rosneft_json_raw
export RESULT_DIR=/data/projects/Rosneft/rosneft_json_raw/

find . -name "*_${1}.log" -type f -delete
python processor/main_raw_json.py -dep $1