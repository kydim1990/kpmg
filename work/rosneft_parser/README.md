# Rosneft Parser
Python 3.7.3

## Install dependences
```
pip instal -r requirements.txt
```

## Environment's variables
```
IS_OVERWRITE - is rewrite result (0/1)
RESULT_DIR - result directory
ROOT_DIR - root dir
```

## Steps parser
# Step 1: excel files to raw json
```
bash run_extract_raw.sh [department list splitted by comma]
```

# Step 2: extract info from raw json
```
bash run_processor_raw.sh [department list splitted by comma] [path_to_parse]
```

## Example: 
```
bash run_processor_raw.sh 29 "/Нефтеюганский филиал РНБ/"
```

# Step 3: join result from separated json to one
```
bash run_join_data.sh [department list splitted by comma]
```

# Step 4: delete tmp folders (folders with end '__json')
```
bash run_remove_dirs_by_mask.sh [mask]
```

----------------------
# Run full pipeline
```
bash run_full_process.sh [department list splitted by comma]
```
