from docx import Document
import xlsxwriter
import os
from utils.logger import Logger
import json

env = os.environ

ROOT_DIR = env.get("ROOT_DIR")
RESULT_DIR = env.get("RESULT_DIR")

class DocxParser(Logger):
    """
    Класс для извлечения данных из docx в jsonlines
    """
    def __init__(self, filename):
        super().__init__()

        self.filename = filename
        self.path_to_save = None

    def iter_unique_cells(self, row):
        """Generate cells in *row* skipping empty grid cells."""
        prior_tc = None
        empty_cell_list = []
        for cell in row.cells:
            this_tc = cell._tc
            if this_tc is prior_tc:
                new_cell = cell
                new_cell.text = ""
                continue
            prior_tc = this_tc
            empty_cell_list.append(cell)
            for i in empty_cell_list:
                yield i
            empty_cell_list = []

    def prepare_dir(self):
        """
        Preparing dir
        :return:
        """
        dir_to_save = os.path.dirname(self.path_to_save).replace(ROOT_DIR, RESULT_DIR)
        if not os.path.exists(dir_to_save):
            os.makedirs(dir_to_save)

    def get_name_to_save(self, path, sheet_title='data'):
        """
        Возвращает имя файла, в который будет сохранен результат
        :param path:
        :param sheet_title:
        :return:
        """
        dir_to_save = os.path.dirname(path).replace(ROOT_DIR, RESULT_DIR)

        new_path = os.path.join(dir_to_save, os.path.basename(path))
        pre, ext = os.path.splitext(new_path)
        p = f'{pre}_json'
        if not os.path.exists(p):
            os.makedirs(p)

        path_to_save = os.path.join(f'{pre}_json', f'{sheet_title}.json')

        self.path_to_save = path_to_save

    def parse(self):
        """
        Парсинг, основная функция
        :return:
        """
        self.get_name_to_save(self.filename)

        document = Document(self.filename)
        with open(self.path_to_save, 'w') as f:
            for table in document.tables:
                for r_num, row in enumerate(table.rows):
                    row_list = {}
                    for c_num, cell in enumerate(self.iter_unique_cells(row)):
                        if len(cell.text.strip()) > 0:
                            letter = xlsxwriter.utility.xl_col_to_name(c_num)
                            coordinate = f'{letter}{r_num+1}'
                            row_list[coordinate] = cell.text

                    if r_num % 50 == 0:
                        print('.', end='')
                    if row_list:
                        row = json.dumps(row_list, ensure_ascii=False)
                        f.write(f'{row}\n')

            f.write(f'END_FILE')
            print('.')

if __name__ == '__main__':
    parser = DocxParser('test.docx')
    parser.parse()
