# KPMG Brunswick
## Run

```bash
python manage.py runserver
```

## Debug configuration

To disable map, charts and hichartjs's settings:

Set `false` in file: `/terminal/static/terminal/js/settings.js`
```js
var IS_INIT_MAP = true;
var IS_LOAD_MAP_TILELAYER= true;
var IS_INIT_HICHARTS = true;
var IS_USE_DEFAULT_CHART = true;
```

##Run docker on 148
```bash
bash run_148.sh
```

##Run docker on local machine
```bash
bash run_dev.sh
```


##Run docker on local machine with ngninx
```bash
bash run_ngninx_dev.sh
```

##Run backend test
```bash
bash run_tests.sh
```