$(document).ready(function () {
   TEST_RESULT = run_tests();
});


function wrap_result(test_elem) {
    let status = test_elem.errors.length ? 'error': 'ok';
    let result = {
        name: test_elem.name,
        status: status,
        errors: test_elem.errors
    };

    return result;
}

function run_tests() {
    let result = {};
    console.log('%c --Run all tests ---', 'color: orange');
    $.each(Tests, function (k, v) {
       console.log('--Run test "', v.name, '"--');
       result[k] = v.func();
       if (result[k].status === 'ok'){
           console.log('%c Passed', 'color: green');
       }
       else{
           console.log('%c Failed', 'color: red');
       }
    });
    console.log('%c --Finish all tests ---', 'color: orange');

    return result;
}

var Tests = {
    default_main_ids_classes: {
        name: 'Main ids and classes',
        func: function () {
            let errors = [];
            let chech_list = {
                ids: TEST_MAIN_IDS,
                classes: TEST_MAIN_CLASS,
            };

            $.each(chech_list, function (i, v) {
                for (let i in v){
                    let _check = i === 'ids' ? document.getElementById(v[i]): document.getElementsByClassName(v[i]);

                    if (_check == null){
                        errors.push('Not found ' + v[i]);
                    }
                }
            });
            this.errors = errors;
            return wrap_result(this);
        }
    },
    // ---------------------
    default_template_search_exists: {
        name: 'Template search exists',
        func: function () {
            this.errors = [];
            if (!$('#hist_panel .search_panel').length) {
                this.errors.push('First chart is not shown');
            }
            return wrap_result(this);

        }
    },
    // ---------------------
    default_chart_list_menu: {
        name: 'Chart list menu',
        func: function () {
            this.errors = [];
            let ch_len = CHARTS_IDS.length;
            if ($('#chart_panel_body .chart_use').length < ch_len) {
                this.errors.push('Count chart less ' + ch_len + ', should be more ' + ch_len);
            }
            return wrap_result(this);
        }
    }

};

//functions to help to testing
function get_vars_main_filter(what_return='all') {
    let elems = $('#d_filter_list .filter_elem:not(.hide)');
    let f_codes = {};
    $.each(elems, function (i, v) {
        let data_id = v.getAttribute('data-id');
        f_codes[data_id] = v.textContent;
    });

    if (what_return === 'keys'){
        return Object.keys(f_codes);
    }
    else if (what_return === 'values'){
        return Object.values(f_codes);
    }
    else{
        return f_codes;
    }

}

function add_all_main_filters(){
    let all_filters = $('#d_filter_list .filter_elem');
    $.each(all_filters, function (i, v) {
        v.click();
    });
}

function disable_all_charts() {
   $('#chart_panel_body .chart_use.active').click();
}

function get_charts_ids() {
    let chart_list = $('#chart_panel_body .chart_use');
    let chart_ids = chart_list.map(function (i, x) {
        return x.getAttribute('data-id');
    });

    return chart_ids;
}