function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function init_map() {
    checkExist1 = setInterval(function() {
       if ($('#mapid').length) {
            clearInterval(checkExist1);
            let stated_point = [55.75126974369615, 80.62405395507813];
            map = new L.Map(
                'mapid',
                {
                    zoom: 4,
                    minZoom: 3,
                    doubleClickZoom: false,
                    zoomControl: false,
                    center: new L.latLng(stated_point),
                }
            );

            let command;

            //add exit fuul size
            command = L.control({position: 'topright'});
            command.onAdd = function () {
                let div = L.DomUtil.create('div', 'map_exit_full_size hide');
                // let html = terminal_templates.legends_template({});
                div.innerHTML = '';
                return div;
            };
            command.addTo(map);

            $('.map_exit_full_size').click(function () {
               //set exit full size
               $('#mapid').removeClass('full_size');
               $('#map_col').removeClass('full_size');
               $('#table_col').removeClass('full_size');

               $('.map_full_size ').removeClass('active hide');
               $('.map_exit_full_size').addClass('hide');

               //full size chart
               $('.map_chart_panel').removeClass('full_size');
               $('.chart_full_size').removeClass('full_size');


               //if first_chart
               if(! $('#line_chart_all').hasClass('hide')){
                   Highcharts.chart('line_chart_all', Charts['line_chart_all']);
                    if (CURRENT_CHART_CONFIG != null){
                       draw_current_chart('chart_full_size_id');
                    }
               }
               else{
                  draw_current_chart('chart_full_size_id');
               }

            });

            //add zoom control
            let control = new L.Control.Zoomslider(
                {
                    position: 'topright'
                }
            ).addTo(map);

            //add legends
            command = L.control({position: 'topright'});
            command.onAdd = function () {
                let div = L.DomUtil.create('div', 'terminal_legends inactive');
                let html = terminal_templates.legends_template({});
                div.innerHTML = html;
                return div;
            };
            command.addTo(map);
            $('#legends_body').html($('#list_terminals_br_color').html());


            $('.terminal_legends').click(function () {
                event.stopPropagation();

                let elem = $('.terminal_legends');
                if (elem.hasClass('inactive')){
                    elem.removeClass('inactive');
                    elem.addClass('active');

                    $('.terminal_legends .body').removeClass('hide');
                }
                else if (event.target.id === 'mark_shown'){
                    elem.removeClass('active');
                    elem.addClass('inactive');

                    $('.terminal_legends .body').addClass('hide');
                }
               // let html = terminal_templates.legends_template({});
               // $('.terminal_legends').html(html);
            });

            //add full size
            command = L.control({position: 'topright'});
            command.onAdd = function () {
                let div = L.DomUtil.create('div', 'map_full_size');
                // let html = terminal_templates.legends_template({});
                div.innerHTML = '';
                return div;
            };
            command.addTo(map);

            $('.map_full_size ').click(function () {
               event.stopPropagation();
               let elem = $('.map_full_size ');
               if (elem.hasClass('active')){

               }
               else{
                   //set full size
                   $('#mapid').addClass('full_size');
                   $('#map_col').addClass('full_size');
                   $('#table_col').addClass('full_size');

                   elem.addClass('active hide');
                   $('.map_exit_full_size').removeClass('hide');

                   //full size chart
                   $('.map_chart_panel').addClass('full_size');
                   $('.chart_full_size').addClass('full_size');

                   //if first_chart
                   if(! $('#line_chart_all').hasClass('hide')){
                       Highcharts.chart('line_chart_all', Charts['line_chart_all']);
                       if (CURRENT_CHART_CONFIG != null){
                           draw_current_chart('chart_full_size_id');
                       }

                   }
                   else{
                      //resize big chart
                      draw_current_chart('chart_full_size_id');
                   }
               }
                map.invalidateSize();
            });

            //Multiple layers (heat layers)
            command = L.control({position: 'topright'});
            command.onAdd = function () {
                let div = L.DomUtil.create('div', 'switch_layer inactive');
                let html = terminal_templates.layer_list_template(MAP_LAEYRS_NAMES);
                div.innerHTML = html;
                return div;
            };
            command.addTo(map);


            $('.switch_layer').click(function () {
                event.stopPropagation();

                let elem = $('.switch_layer');
                if (elem.hasClass('inactive')){
                    elem.removeClass('inactive');
                    elem.addClass('active');

                    $('.switch_layer .body').removeClass('hide');
                }
                else{
                    elem.removeClass('active');
                    elem.addClass('inactive');

                    $('.switch_layer .body').addClass('hide');
                }
               // let html = terminal_templates.legends_template({});
               // $('.terminal_legends').html(html);
            });

            //================LEFT panel======

            //add select terminals
            command = L.control({position: 'topleft'});
            command.onAdd = function () {
                let div = L.DomUtil.create('div', 'map_select_terminals select_mode');
                div.innerHTML = $('#list_terminals_br').html();
                return div;
            };
            command.addTo(map);

            // add select mode terminals
            command = L.control({position: 'topleft'});
            command.onAdd = function () {
                let div = L.DomUtil.create('div', 'map_select_mode_terminals select_mode');

                let context = {
                    id: "dd_select_mode_terminals",
                    name: "Выберите тип",
                    vars: terminal_type_mode

                };
                let html = terminal_templates.select_terminals_template(context);
                div.innerHTML = html;
                return div;
            };
            command.addTo(map);

            //----------------

            $('.map_select_mode_terminals').click(function () {
               let front = $(event.target);
               let elem = front.parents('.select_mode')[0];
               elem = $(elem);
               if (elem.hasClass('active')){
                   //hide terminals
                   elem.removeClass('active');
                   elem.find('.list_terminals').addClass('hide');
               }
               else{
                   //show terminals
                   elem.addClass('active');
                   elem.find('.list_terminals').removeClass('hide');
               }
            });

            //add bind with stations
            command = L.control({position: 'topleft'});
            command.onAdd = function () {
                let div = L.DomUtil.create('div', 'bind_with_stations select_mode hide');

                let context = {
                    id: "tg_bind_with_stations",
                    name: "Связь с ЖД",
                };
                let html = terminal_templates.bind_with_stations_template(context);
                div.innerHTML = html;
                return div;
            };
            command.addTo(map);

            //add chart panel ========
            command = L.control({position: 'bottomleft'});
            command.onAdd = function () {
                let div = L.DomUtil.create('div', 'map_chart_panel hide');

                let html = terminal_templates.chart_panel_template(CHARTS_IDS);
                div.innerHTML = html;
                return div;
            };
            command.addTo(map);

            // Disable dragging when user's cursor enters the element
            command.getContainer().addEventListener('mouseover', function () {
                map.dragging.disable();
                map.scrollWheelZoom.disable();
            });

            // Re-enable dragging when user's cursor leaves the element
            command.getContainer().addEventListener('mouseout', function () {
                map.dragging.enable();
                map.scrollWheelZoom.enable();
            });

            let x,y,top,left,down;

            $(".map_chart_panel").mousedown(function(e){
                e.preventDefault();
                down=true;
                x=e.pageX;
                y=e.pageY;
                top=$(this).scrollTop();
                left=$(this).scrollLeft();
            });

            $("body").mousemove(function(e){
                if(down){
                    let newX=e.pageX;
                    let newY=e.pageY;

                    $(".map_chart_panel").scrollTop(top-newY+y);
                    $(".map_chart_panel").scrollLeft(left-newX+x);
                }
            });

            $("body").mouseup(function(e){down=false;});

            // mouse wheel
            (function() {
                function scrollHorizontally(e) {
                    e = window.event || e;
                    let delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
                    let scrollLeft = $(".map_chart_panel")[0].scrollLeft;
                    console.log('delta: ', delta);
                    console.log('scrollLeft: ', scrollLeft);
                    $(".map_chart_panel").scrollLeft(scrollLeft - delta * 100); // Multiplied by
                    e.preventDefault();
                }

                command.getContainer().addEventListener("mousewheel", scrollHorizontally, false);
            })();

            // / add chart panel ========

            //add frame for selected chart
            command = L.control({position: 'bottomleft'});
            command.onAdd = function () {
                let div = L.DomUtil.create('div', 'chart_full_size hide');
                let html = terminal_templates.chart_full_size_template({});
                div.innerHTML = html;
                return div;
            };
            command.addTo(map);

            $('.chart_full_size .close_chart').click(function () {
               $('.chart_full_size').addClass('hide');
            });

            //map
            if (IS_LOAD_MAP_TILELAYER){
                L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
                    attribution: '',
                }).addTo(map);
            }

            // add heat map
           if (IS_USE_HEAT_MAP) {
               $.ajax({
                   type: 'POST',
                   url: '/get_heat_map',
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   data: JSON.stringify({
                       "date_from": Filters['data_otpravlenija'].val["from"],
                       "date_to": Filters['data_otpravlenija'].val["to"]
                   }),
                   success: function (data) {
                       HEAT_MAP_LAYER = L.heatLayer(data, {
                           radius: 70,
                           blur: 70,
                           maxZoom: 1
                       })
                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       console.log('Error get_br_goods', xhr.status, thrownError);
                   }
               });
           }

            //add lines
            //
            add_lines();

            //
            default_point();

            //load_sea_terminals();
       }
    }, 100);

}

function addpanel() {
    let tmp_temlp = terminal_templates.panel_template({});
    $('#panel').html(tmp_temlp);
}

function close_popup(el) {
    $(".leaflet-popup-close-button")[0].click();
}

//routing
function clear_lines() {
    $.each(Layer_lines_group.getLayers(), function (k, v) {
        map.removeLayer(v);
    })
}

function add_lines(data=null) {
    let lines = [], routing, w, color;
    if (true){
        $.ajax({
          type: 'POST',
          url: '/get_lines_by_filters',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          data: JSON.stringify(data),

          success: function(dataset){
              if (jQuery.isEmptyObject(dataset)){
                  console.log('Enpty');
                  alert('Нет данных о маршрутах с этими параметрами!');
                  return;
              }

              //add stations
              load_stations_from_data(dataset.rail_station);

              //add routings
              data = dataset.routing;

              let list_ranges = [];
              for (let j in data){
                  lines = [];

                  routing = data[j]['value'];
                  w = data[j]['class'];
                  console.log('%c ' + 'routing: ' + routing.length, 'color: green');

                  for (let i in routing){
                      lines.push(JSON.parse(routing[i]['geojson']));
                  }

                  let g = L.geoJSON(lines, {
                      style: {
                           "color": RAINBOW[w-1],
                           "weight": w * 0.5,
                           "opacity": 0.5,
                           "interactive": false
                      }
                  }).addTo(map);
                  Layer_lines_group.addLayer(g);

                  let range =  data[j]['range'];
                  let range_str = range[0] + ' - ' + range[1];

                  list_ranges.push(
                      {
                          color: RAINBOW[w-1],
                          range: range_str,
                          sum_weight: data[j]['sum_value']
                      }
                  );

              }

            if (jQuery.isEmptyObject(list_ranges)){
                  console.log('Enpty');
                  alert('Нет данных с этими параметрами!');
                  return;
              }

            //add list routing
            let common_count = data.reduce((prev, cur) => prev + cur.sum_value, 0);
            let res = {
                list_ranges: list_ranges,
                common_count: numberWithCommas(common_count),
                full_sum_value: numberWithCommas(dataset.full_sum_value)
            };

            let tmp_temlp = terminal_templates.list_routing_template(res);
            // console.log(tmp_temlp);
            $('#list_routing').html(tmp_temlp);

             console.log('%c finish routing', 'color: green');
             show_chart();
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log('Error get_routing', xhr.status, thrownError);
          }
        });
    }

}


function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function stop_map_wheel() {
    event.stopPropagation();
}

function load_stations_from_data(data) {
    let icon = {
        color: 'darkmagenta',
        fillColor: 'darkmagenta',
        fillOpacity: 1,
        radius: 3
      };
    let circleMarker;
    $.each(data, function (k, v) {
        // console.log('add ' + k);
        circleMarker = L.circleMarker([v['lat'], v['lon']], icon).addTo(map).bindPopup(v['name'])
            .on('mouseover', function (e) {
                this.openPopup();
            }).on('mouseout', function (e) {
                this.closePopup();
            })
        ;
        Layer_stations_group.addLayer(circleMarker);

    });
    let tmp_temlp = terminal_templates.stations_template(data);
    $('.point').html(tmp_temlp);
    // debugger
    console.log('%c finish rail stations', 'color: green');
}


function default_point(config){
    $.ajax({
      type: 'GET',
      url: '/get_terminals',

      success: function(datasets){
          let tmp_temlp, temp_prop, terminals = [], geojson;
          //add stations
          $.each(Layer_stations_group.getLayers(), function (k, v) {
                v.addTo(map);
          });
          for (let i in datasets) {
              if (config) {
                  if (config.name !== 'all'){
                      if (config.name === 'rail_stations'){
                          break;
                      }
                      else if (config.name != datasets[i].code) {
                          continue;
                      }
                  }
              }

              terminals.push(
                  {
                      name: datasets[i].code,
                      color: datasets[i].color,
                      owner: datasets[i].owner,
                      type: datasets[i].type_road
                  }
              );

              try{
                  geojson = JSON.parse(datasets[i].geojson);
              }
              catch {
                  console.log('Error get geojson');
              }

              let marker_ = new L.Marker.SVGMarker([0, 0],
                  {
                      iconOptions: {
                          color: datasets[i].color,
                          fillOpacity: 1,
                          iconSize: ICON_SIZE
                      }
                  });
              let icon = marker_.options.icon;
              let g = L.geoJSON(geojson, {
                onEachFeature: function (f, l) {
                   l.setIcon(icon);
                   let railway_station = f.properties.railway_station == null ?
                       null : f.properties.railway_station.toUpperCase();

                   temp_prop = {
                       owner: datasets[i].owner,
                       place: f.properties.place,
                       description: f.properties.description,
                       railway_station: railway_station,
                       teu_per_month: datasets[i].teu_per_month
                   };
                   tmp_temlp = terminal_templates.description_template(temp_prop);

                   l.bindPopup(tmp_temlp);
                }
              }).addTo(map);

              Layer_all_terminals.addLayer(g);
              all_terminals_list.push({
                  layer: g,
                  code: datasets[i].code
              });
              if (datasets[i].type_road === 'sea') {
                  Layer_sea_terminals.addLayer(g);
              } else {
                  Layer_railway_terminals.addLayer(g);
              }
          }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log('Error get_terminals', xhr.status, thrownError);
      }
    });

}

function clear_points(is_delete=false) {
    $.each(Layer_all_terminals.getLayers(), function (k, v) {
        map.removeLayer(v);
    });
    if (is_delete){
        Layer_railway_terminals = L.layerGroup();
        Layer_sea_terminals = L.layerGroup();
        Layer_all_terminals = L.layerGroup();
    }
}

function draw_terminal_by_owners(ids=['all']) {
    if (ids.indexOf("all") !== -1) {
        $.each(all_terminals_list, function (k, v) {
            v.layer.addTo(map);
        });
    } else {
        $.each(all_terminals_list, function (k, v) {
            if (ids.indexOf(v.code) !== -1) {
                v.layer.addTo(map);
            }
        })
    }
}

function draw_terminal_by_mode(type='all') {
    $('.mode_terminals.active').removeClass('active');
    $('.mode_terminals[data-id="' + type + '"]').addClass('active');

    switch (type) {
        case 'sea':
            $.each(Layer_sea_terminals.getLayers(), function (k, v) {
               v.addTo(map);
            });
            break;
        case 'rail':
            $.each(Layer_railway_terminals.getLayers(), function (k, v) {
               v.addTo(map);
            });
            break;
        default:
            $.each(Layer_all_terminals.getLayers(), function (k, v) {
               v.addTo(map);
            });
    }

    //hide terminal dependens on type
    $.each(TERMINALS_JSON, function (i, v) {
        if (type === 'all' || type === v.type_road){
            v.is_hide = false;
            $('.terminal_legend[data-id="' + v.code + '"]').removeClass('hide');
            //show terminals
        }
        else{
            v.is_hide = true;
            $('.terminal_legend[data-id="' + v.code + '"]').addClass('hide');
            //hide terminals
        }
    });
}


function clear_stations(is_delete=false) {
    $.each(Layer_stations_group.getLayers(), function (k, v) {
        map.removeLayer(v);
    });
    if (is_delete) {
       Layer_stations_group = L.layerGroup();
    }
}

function onchange_filter(el) {
    //clear map
    clear_points();
    clear_stations();

    default_point({
        name: el.value
    })
}

function onclick_by_mode() {
    clear_points();
    draw_terminal_by_mode(event.target.getAttribute('data-id'));
}

function onclick_by_owner() {
    event.stopPropagation();

    let clicked_owner_code = event.target.getAttribute('data-id');

    if (clicked_owner_code === "hide_all"){
        SELECTED_OWNERS = [];
        $('#d_filter_terminals div[data-id="hide_all"]').addClass('terminal_checked');
        $('#d_filter_terminals div:not([data-id="hide_all"])').removeClass('terminal_checked');
    }
    else{
        $('#d_filter_terminals div[data-id="hide_all"]').removeClass('terminal_checked');

        if (clicked_owner_code === "all") {
            SELECTED_OWNERS = [];
            $('#d_filter_terminals div[data-id="all"]').addClass('terminal_checked');
        } else {
            $('#d_filter_terminals div[data-id="all"]').removeClass('terminal_checked');
            let allindx = SELECTED_OWNERS.indexOf("all");
            if (allindx !== -1) {
                SELECTED_OWNERS.splice(allindx, 1);
            }
        }

        let indexof = SELECTED_OWNERS.indexOf(clicked_owner_code);
        if (indexof === -1) {
            SELECTED_OWNERS.push(clicked_owner_code);
            if (clicked_owner_code === "all"){
                $('#d_filter_terminals div.terminal_checked').removeClass('terminal_checked');
            }
            else{
                $('#d_filter_terminals div[data-id="' + clicked_owner_code + '"]').addClass('terminal_checked');
            }

        } else {
            SELECTED_OWNERS.splice(indexof, 1);
            $('#d_filter_terminals div[data-id="' + clicked_owner_code + '"]').removeClass('terminal_checked');
        }
    }

    clear_points();
    draw_terminal_by_owners(SELECTED_OWNERS);
}

function draw_heatmap() {
    $.ajax({
        type: 'POST',
        url: '/get_heat_map',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
            "date_from": Filters['data_otpravlenija'].val["from"],
            "date_to": Filters['data_otpravlenija'].val["to"]
        }),
        success: function (data) {
            map.removeLayer(HEAT_MAP_LAYER);
            HEAT_MAP_LAYER = L.heatLayer(data, {
                radius: 70,
                blur: 70,
                maxZoom: 1
            }).addTo(map);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('Error get_br_goods', xhr.status, thrownError);
        }
    });
}

function onclick_by_map_layer() {
    event.stopPropagation();
    if (event.target.getAttribute('data-id') === "heat") {
        draw_heatmap();
        $('#map_layer_list_body [data-id="heat"]').addClass('active');
        $('#map_layer_list_body [data-id="simple"]').removeClass('active');
    } else {
        map.removeLayer(HEAT_MAP_LAYER);
        $('#map_layer_list_body [data-id="simple"]').addClass('active');
        $('#map_layer_list_body [data-id="heat"]').removeClass('active');
    }
}

$( document ).ready(function() {
    //
    if (IS_INIT_MAP){
        init_map();
    }
});

