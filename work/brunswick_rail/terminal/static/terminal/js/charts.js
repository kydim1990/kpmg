
$(document).ready(function () {

});

// ----------------------------------------
function draw_pie(id, chart) {
    // Build the chart
    let subtitle = chart.data.length ? '': 'Данные не найдены!';

    let config = {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
        },
        title: {
            text: chart.name
        },
        subtitle: {
            text: subtitle
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },

        series: [{
            showInLegend: false,
            name: chart.name,
            colorByPoint: true,
            data: chart.data
        }]
    };
    Highcharts.chart(id, config);
    Charts[id] = config;
}


function draw_line_chart(id, chart, chart_y_title){
    let data = [], title;

    if ('all' in chart){
        title = '';

        let c = chart.data.map(a => [Date.UTC(a.year, a.month - 1, a.day), a.value]);
        data.push(
            {
                showInLegend: false,
                name: chart.name,
                data: c
            }
        );
    }
    else{
        title = chart.name;

        $.each(chart.data, function (i, d) {
            let c = d.map(a => [Date.UTC(a.year, a.month - 1, a.day), a.value]);

            data.push(
                {
                    showInLegend: false,
                    name: i,
                    data: c
                }
            );
        });
    }

    let legend_layout = data.length > 15 ? 'vertical': 'proximate';
    let subtitle = data.length ? '': 'Данные не найдены!';

    let config = {
        title: {
            text: title
        },
        subtitle: {
            text: subtitle
        },
        yAxis: {
            // labels: {
            //     enabled: false,
            // },
            type: 'logarithmic',
            title: {
                text: chart_y_title
            },
            visible: false
        },
        legend: {
            layout: legend_layout,
            align: 'right',
            verticalAlign: 'middle',
            itemStyle: {
                width: 200
            }
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
            }
        },
        xAxis: {
            type: 'datetime',
            labels: {
                format: '{value:%b-%Y}',
            },
            visible: false
        },

        series: data,

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    };

    //change color for first chart
    if (id === 'line_chart_all'){
        config.colors = ['#43B02A'];
    }


    Highcharts.chart(id, config);
    Charts[id] = config;
}


// ---------PIE-------------------
function get_charts_pie(config, data=Filters) {
    let copy_data = Object.assign({}, data);

    copy_data.chart_name = config.chart_name;
    copy_data.chart_type = config.chart_type;

    let _id = 'pie_' + copy_data.chart_name;

    $.ajax({
      type: 'POST',
      url: '/get_charts',
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: JSON.stringify(copy_data),

      success: function(d){
          draw_pie(_id, d);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log('Error get_routing', xhr.status, thrownError);
      }
    });
}

// ---------LINE CHART-------------------

function get_charts_line(config, data=Filters) {
    let copy_data = Object.assign({}, data);
    copy_data.chart_name = config.chart_name;
    copy_data.chart_type = config.chart_type;

    let _id = 'line_chart_' + copy_data.chart_name;

    $.ajax({
      type: 'POST',
      url: '/get_charts',
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: JSON.stringify(copy_data),

      success: function(d){
          draw_line_chart(_id, d, config.chart_y_title);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log('Error get_routing', xhr.status, thrownError);
      }
    });
}

// ---------SHOW CHART-------------------
function show_chart() {
    $('.map_chart_panel').removeClass('hide');
}


//full size chart
function click_chart_one() {
    $('.chart_full_size').removeClass('hide');
    $('.close_chart').removeClass('hide');
    let elem = $(event.target)[0];
    let chart_one = $(elem).parents('.chart_one')[0];

    //move scroll
    // let left_offset = chart_one.offsetLeft;
    // $('.map_chart_panel').scrollLeft(left_offset - 50);
    // $('.map_chart_panel').animate({scrollLeft(left_offset)}, 800);

    //set full size
    let config = Charts[chart_one.id];

    if (chart_one.id !== 'line_chart_all'){
       $.each(config.series, function (i, v) {
            v.showInLegend = true;
       });
    }


    if ( !('pie' in config.plotOptions)){
        config.yAxis.visible = true;
        config.xAxis.visible = true;
    }

    CURRENT_CHART_CONFIG = {
        full_size: config,
        short_size: Charts[chart_one.id]
    };

    Highcharts.chart('chart_full_size_id', config);
}


//arrows events

function arrow_click() {
    let cur_scroll = $('.map_chart_panel').scrollLeft();
    let step = ARROW_STEP;
    if (event.target.alt === 'arrow_left'){
        step = -step;
    }

    $('.map_chart_panel').animate({scrollLeft: cur_scroll + step}, 0);
}

function draw_current_chart(id, size='full_size') {
    Highcharts.chart(id, CURRENT_CHART_CONFIG[size]);
}