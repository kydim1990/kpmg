DEFAULT_INDICATOR = "sum_teu"

INDICATORS_DEFAULT_HAVING = {
    "sum_teu": {"having": 1000},
    "count_rows": {"having": 1000},
    "sum_obem_perevozok_tn": {"having": 5},
}

INDICATOR_AS = [
    {"code": "sum_teu", "value": "ДФЭ"},
    {"code": "count_rows", "value": "Вагоноотправки"},
    {"code": "sum_obem_perevozok_tn", "value": "Тонны"},
]

OTHERS = "Прочее"
ALL_DICTS_NONE = "Не определено"
# draw: -1 - not draw, 0 -hide, 1 -draw
ALL_DICTS_LIST = {
    "fakt_data_pribytija": {
        "name": "Фактическая дата прибытия",
        "type": "date",
        "order": 0,
        "draw_chart": -1,
    },
    "type_vagon": {
        "name": "Тип платформы",
        "type": "list",
        "vars": {},
        "order": 1,
        "draw_chart": 1,
    },
    "tip_rejsa": {
        "name": "Тип рейса",
        "type": "list",
        "vars": {},
        "order": 2,
        "draw_chart": 1,
    },
    # "tip_transportirovki": {
    #     "name": "Тип транспортировки",
    #     "type": "list",
    #     "vars": {},
    #     "order": 3,
    #     "draw_chart": 0,
    # },
    "tip_soobshhenija": {
        "name": "Тип транспортировки",
        "type": "list",
        "vars": {},
        "order": 3,
        "draw_chart": 0,
    },
    "kategorija_otpravki": {
        "name": "Категория отправки",
        "type": "list",
        "vars": {},
        "order": 4,
        "draw_chart": 1,
    },
    "kod_gruza": {
        "name": "Код груза",
        "type": "list",
        "vars": {},
        "order": 5,
        "draw_chart": 0,
    },
    "gruppa_gruza": {
        "name": "Группа груза",
        "type": "list",
        "vars": {},
        "order": 6,
        "draw_chart": 0,
    },
    "podgruppa_gruza": {
        "name": "Подгруппа груза",
        "type": "list",
        "vars": {},
        "order": 7,
        "draw_chart": 0,
    },
    "nom_good_group_name": {
        "name": "Номенклатура груза",
        "type": "list",
        "vars": {},
        "order": 8,
        "draw_chart": 0,
    },
    "gosudarstvo_otpravlenija": {
        "name": "Государство отправления",
        "type": "list",
        "vars": {},
        "order": 9,
        "draw_chart": 0,
    },
    "doroga_otpravlenija": {
        "name": "Дорога отправления",
        "type": "list",
        "vars": {},
        "order": 10,
        "draw_chart": 0,
    },
    "kod_stancii_otpravlenija_rf": {
        "name": "Станция отправления РФ",
        "type": "list",
        "vars": {},
        "order": 11,
        "draw_chart": 0,
    },
    "gruzootpravitel": {
        "name": "Грузоотправитель",
        "type": "list",
        "vars": {},
        "order": 12,
        "draw_chart": 0,
    },
    "gruzopoluchatel": {
        "name": "Грузополучатель",
        "type": "list",
        "vars": {},
        "order": 13,
        "draw_chart": 0,
    },
    "gosudarstvo_naznachenija": {
        "name": "Государство назначения",
        "type": "list",
        "vars": {},
        "order": 14,
        "draw_chart": 0,
    },
    "doroga_naznachenija": {
        "name": "Дорога назначения",
        "type": "list",
        "vars": {},
        "order": 15,
        "draw_chart": 0,
    },
    "kod_stancii_naznachenija_rf": {
        "name": "Станция назначения РФ",
        "type": "list",
        "vars": {},
        "order": 16,
        "draw_chart": 0,
    },
    "tonnazhnost_kontejnera_and_razmer": {
        "name": "Размер контейнера",
        "type": "list",
        "vars": {},
        "order": 17,
        "draw_chart": 0,
    },
    "vid_kontejnera": {
        "name": "Вид контейнера",
        "type": "list",
        "vars": {},
        "order": 18,
        "draw_chart": 1,
    },
    "gosudarstvo_sobstvennik_vagona": {
        "name": "Государство собственник вагона",
        "type": "list",
        "vars": {},
        "order": 19,
        "draw_chart": 0,
    },
    "uchastnik_rynka": {
        "name": "Участник рынка",
        "type": "list",
        "vars": {},
        "order": 20,
        "draw_chart": 0,
    },
}

CHARTS_LIST = {
    d: ALL_DICTS_LIST[d]["name"]
    for d in ALL_DICTS_LIST
    if ALL_DICTS_LIST[d]["draw_chart"] > -1
}
ALL_DATA_CHART_NAME = "Динамика рынка железнодорожных контейнерных перевозок, ДФЭ"
CHARTS_LIST.update({"all": ALL_DATA_CHART_NAME})
