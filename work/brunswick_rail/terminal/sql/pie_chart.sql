SELECT dict.value as name, sum({indicator}) AS y
FROM br_front AS main
JOIN br_dict_{column} AS dict
  ON main.{column} = dict.id 
{where_clause}
GROUP BY dict.value
HAVING sum({indicator}) > {having}
ORDER BY y;