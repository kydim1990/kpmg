SELECT dict.value as name, 
       to_date(to_char(data_otpravlenija, 'mm/yy'), 'mm.yy') as date, 
       sum({indicator}) as value
FROM br_front as main
JOIN br_dict_{column} AS dict
ON main.{column} = dict.id
{where_clause}
GROUP BY dict.value, to_date(to_char(data_otpravlenija, 'mm/yy'), 'mm.yy')
HAVING sum({indicator}) > {having}
ORDER BY date;