SELECT max(lat) AS lat,
       max(lon) AS lon,
       avg(value) / max(TEU_per_month) AS intensity
FROM br_heatmap
WHERE br_heatmap.date >= '{date_from}' AND br_heatmap.date < '{date_to}'
AND TEU_per_month > 0
GROUP BY br_heatmap.name;