SELECT to_date(to_char(data_otpravlenija, 'mm/yy'), 'mm.yy') AS date,
       sum({indicator}) AS value
FROM br_front
WHERE data_otpravlenija >= '{date_from}' AND  data_otpravlenija < '{date_to}'
GROUP BY date
HAVING sum({indicator}) > {having}
ORDER BY date;