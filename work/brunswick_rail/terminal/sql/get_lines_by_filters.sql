SELECT g.p1         AS p1,
       g.p2         AS p2,
       g.geojson    AS geojson,
       CAST(sum({indicator}) AS INTEGER) AS value
FROM br_front f
    LEFT JOIN br_geo_routing g 
        ON f.geo_json = g.id
{where_clause}
GROUP BY 1, 2, 3
HAVING sum({indicator}) > {having};