import json
import os
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from distutils.util import strtobool
from terminal import log
from terminal.utils import (del_filter, get_charts, get_context_for_index,
                            get_heat_map, get_lines_by_filters, get_terminals,
                            load_filters, save_filter, get_vars_front)


@csrf_exempt
def index(request):
    context = get_context_for_index()
    # add debug
    context['use_test'] = strtobool(os.environ.get('DEBUG', 'True'))
    return render(request, "terminal/index.html", context)


@csrf_exempt
def vget_terminals(request):
    ts = get_terminals()
    return HttpResponse(ts, content_type="application/json; encoding=utf-8")


@csrf_exempt
def vget_vars_front(request):
    res = get_vars_front()
    return HttpResponse(res, content_type="application/json; encoding=utf-8")


@csrf_exempt
def vsave_filter(request):
    kwargs = json.loads(request.body)
    res = save_filter(**kwargs)
    return HttpResponse(res, content_type="application/json; encoding=utf-8")


@csrf_exempt
def vload_filters(request):
    res = load_filters()
    return HttpResponse(res, content_type="application/json; encoding=utf-8")


@csrf_exempt
def vdel_hist(request, id):
    res = del_filter(_id=id)
    return HttpResponse(res, content_type="application/json; encoding=utf-8")


@csrf_exempt
def vget_lines_by_filters(request):
    content = json.loads(request.body)
    kwargs = content if content is not None else {}
    routings = get_lines_by_filters(**kwargs)
    return HttpResponse(routings, content_type="application/json; encoding=utf-8")


@csrf_exempt
def vget_heat_map(request):
    content = json.loads(request.body)
    kwargs = content if content is not None else {}
    heat_map_data = get_heat_map(**kwargs)
    return HttpResponse(heat_map_data, content_type="application/json; encoding=utf-8")


@csrf_exempt
def vget_charts(request):
    content = json.loads(request.body)
    kwargs = content if content is not None else {}
    chart_data = get_charts(**kwargs)
    return HttpResponse(chart_data, content_type="application/json; encoding=utf-8")
