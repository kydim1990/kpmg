import pkg_resources
import time

from terminal import log
from terminal.config import DEFAULT_INDICATOR, INDICATORS_DEFAULT_HAVING


class DatabaseWrapper:
    def __init__(self, connection):
        self.connection = connection

    def execute(self, query, query_type='select'):
        data = []
        with self.connection.cursor() as cursor:
            start_time = time.time()
            cursor.execute(query)
            if query_type == 'select':
                rows = cursor.fetchall()
                data = [
                    {k[0]: v for k, v in zip(cursor.description, data)} for data in rows
                ]
            ms = (time.time() - start_time) * 1000
            log.debug("Successfully executed query:\n{}\nExecution time: {:.2f} ms".format(query, ms))
            
        return data


class DatabaseQueries(DatabaseWrapper):
    def __init__(self, connection):
        self._templates = self.load_templates()
        super().__init__(connection)

    def load_templates(self):
        templates = {}
        for file_name in pkg_resources.resource_listdir(__name__, "sql"):
            log.debug('Loading sql template: "{}"'.format(file_name))
            resource_path = "/".join(("sql", file_name))
            template = pkg_resources.resource_string(__name__, resource_path)
            templates[file_name.replace(".sql", "")] = template.decode("utf-8")
        return templates

    def _construct_where_clause(self, **kwargs):
        where_clause = []
        for name in kwargs:
            if isinstance(kwargs[name], dict):
                if kwargs[name].get("type") == "list":
                    vals = ["'{}'".format(x) for x in kwargs[name].get("val")]
                    if len(vals) > 0:
                        where_clause.append(
                            "{col} IN ({vals})".format(col=name, vals=", ".join(vals))
                        )
                elif kwargs[name].get("type") == "date":
                    date_from = kwargs[name].get("val").get("from")
                    date_to = kwargs[name].get("val").get("to")
                    where_clause.append(
                        "{col} >= '{dfrom}' AND  {col} < '{dto}'".format(
                            col=name, dfrom=date_from, dto=date_to
                        )
                    )
                else:
                    pass
        res = " AND ".join(where_clause) if where_clause else ""
        return res

    def _get_indicator_and_having(self, **kwargs):
        indicator = kwargs.get("indicator", {}).get("val", DEFAULT_INDICATOR)
        having = kwargs.get("having", {}).get(
            "val", INDICATORS_DEFAULT_HAVING[DEFAULT_INDICATOR]["having"]
        )
        return indicator, having

    def get_pie_chart(self, chart_name, **kwargs):
        log.debug("Executing 'get_pie_chart' query")
        where_clause = self._construct_where_clause(**kwargs)
        if where_clause:
            where_clause = "WHERE " + where_clause
        indicator, having = self._get_indicator_and_having(**kwargs)
        template = self._templates["pie_chart"]
        column = chart_name

        query = template.format(
            column=column, 
            indicator=indicator, 
            having=having, 
            where_clause=where_clause
        )
        rows = self.execute(query)
        return rows

    def get_all_chart(self, **kwargs):
        log.debug("Executing 'get_all_chart' query")
        indicator, having = self._get_indicator_and_having(**kwargs)
        template = self._templates["all_stat"]
        date_from = kwargs.get("data_otpravlenija").get("val").get("from")
        date_to = kwargs.get("data_otpravlenija").get("val").get("to")
        query = template.format(
            indicator=indicator, having=having, date_from=date_from, date_to=date_to
        )
        rows = self.execute(query)
        return rows

    def get_line_chart(self, **kwargs):
        log.debug("Executing 'get_line_chart' query")
        where_clause = self._construct_where_clause(**kwargs)
        if where_clause:
            where_clause = "WHERE " + where_clause
        indicator, having = self._get_indicator_and_having(**kwargs)
        template = self._templates["line_chart"]
        query = template.format(
            column=kwargs.get("chart_name"),
            indicator=indicator,
            having=having,
            where_clause=where_clause,
        )
        rows = self.execute(query)
        return rows

    def get_terminals_query(self):
        log.debug("Executing get_terminals query")
        rows = self.execute(self._templates["get_terminals"])
        return rows

    def get_stations_by_short_codes(self, codes):
        log.debug("Executing get_stations_by_short_codes query")
        template = self._templates["get_stations_by_short_codes"]
        codes = ", ".join(map("'{}'".format, codes))
        query = template.format(short_codes=codes)
        result = self.execute(query)
        return result

    def get_lines_by_filters(self, **kwargs):
        log.debug("Executing get_lines_by_filters: {}".format(kwargs))
        where_clause = self._construct_where_clause(**kwargs)
        if where_clause:
            where_clause = f'where {where_clause}'
        indicator, having = self._get_indicator_and_having(**kwargs)
        query = self._templates["get_lines_by_filters"].format(
            indicator=indicator, 
            having=having, 
            where_clause=where_clause
        )
        result = self.execute(query)
        return result

    def heatmap_query(self, date_from, date_to):
        """Returns list of (lat, lon, intensity)"""
        log.debug("Executing heatmap_query")

        def normalize_data(data):
            res = []
            min_v = min(data)
            maxmin_v = max(data) - min_v
            return [(v - min_v) / maxmin_v for v in data]

        query = self._templates["heatmap"].format(date_from=date_from, date_to=date_to)
        rows = self.execute(query)

        result = []
        if len(rows) > 0:
            for row in rows:
                if row["intensity"] <= 1:
                    result.append([row["lat"], row["lon"], row["intensity"]])

        norm_intensities = normalize_data([data[2] for data in result])
        for data, norm_int in zip(result, norm_intensities):
            data[2] = norm_int

        return result

    def get_all_dicts(self):
        log.debug("Executing 'get_all_dicts' query")
        template = self._templates["all_dicts"]
        rows = self.execute(template)
        return rows
    
    def filter_delete_by_id(self, filter_id):
        log.debug("Executing 'filter_delete_by_id' query")
        template = self._templates["filter_delete_by_id"]
        self.execute(template.format(filter_id=filter_id), query_type='update')
        return 1
    
    def filter_insert_new(self, filter_name, filter_data): 
        log.debug("Executing 'filter_insert_new' query")
        template = self._templates["filter_insert_new"]
        self.execute(template.format(name=filter_name, data=filter_data), query_type='update')
        return 1
    
    def filter_get_id_by_name(self, filter_name):
        log.debug("Executing 'filter_get_id_by_name' query")
        template = self._templates["filter_get_id_by_name"]
        rows = self.execute(template.format(filter_name=filter_name))
        return rows
    
    def filter_load_all(self):
        log.debug("Executing 'filter_load_all' query")
        template = self._templates["filter_load_all"]
        rows = self.execute(template)
        return rows
    
