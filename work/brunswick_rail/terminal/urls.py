from django.urls import path

from terminal import views

urlpatterns = [
    path("", views.index, name="index"),
    path("get_terminals", views.vget_terminals, name="vget_terminals"),
    path("get_heat_map", views.vget_heat_map, name="vget_heat_map"),
    path("get_vars_front", views.vget_vars_front, name="vget_vars_front"),

    path("get_lines_by_filters", views.vget_lines_by_filters, name="vget_lines_by_filters"),
    path("get_charts", views.vget_charts, name="vget_charts"),

    path("save_filter", views.vsave_filter, name="vsave_filter"),
    path("load_filters", views.vload_filters, name="vload_filters"),
    path("del_hist/<int:id>/", views.vdel_hist, name="vdel_hist"),
]
