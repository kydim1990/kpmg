import json
import math
from decimal import Decimal
from functools import lru_cache

from django.db import connection

from terminal import log
from terminal.config import (ALL_DATA_CHART_NAME, ALL_DICTS_LIST,
                             ALL_DICTS_NONE, CHARTS_LIST, DEFAULT_INDICATOR,
                             INDICATOR_AS, INDICATORS_DEFAULT_HAVING, OTHERS)
from terminal.database import DatabaseQueries

queries = DatabaseQueries(connection)


def get_context_for_index():
    sorted_all_filter_keys = sorted(
        ALL_DICTS_LIST, key=lambda x: ALL_DICTS_LIST[x]["order"]
    )

    sorted_all_filter = [
        {
            'code': i,
            'name': ALL_DICTS_LIST[i]['name'],
            'draw_chart': ALL_DICTS_LIST[i]["draw_chart"],
        }
        for i in sorted_all_filter_keys
    ]
    default_from = INDICATORS_DEFAULT_HAVING[DEFAULT_INDICATOR]["having"]
    terminals = get_terminals(is_dict=True)

    charts_id = [i for i in CHARTS_LIST.keys() if i != "all"]
    charts_selected = [i for i in sorted_all_filter if i['draw_chart'] > -1]

    date_templates = [i for i in ALL_DICTS_LIST.values() if i['type'] == 'date']

    context = {
        "INDICATORS_DEFAULT_HAVING": _dump_json(INDICATORS_DEFAULT_HAVING),
        "default_from": default_from,
        "INDICATOR_AS": INDICATOR_AS,
        "INDICATOR_AS_JSON": _dump_json(INDICATOR_AS),
        "DEFAULT_INDICATOR": DEFAULT_INDICATOR,
        "terminals": terminals,
        "terminals_json": _dump_json(terminals),
        "CHARTS_IDS": charts_id,
        "sorted_all_filter": sorted_all_filter,
        "charts_selected": _dump_json(charts_selected),
        "date_templates": date_templates,
    }

    return context


def get_vars_front():
    result = {
        'all_filters': get_all_dicts()
    }
    return _dump_json(result)


def _dump_json(obj):
    def default(obj):
        """Заменяем тип Decimal в методе json.dumps"""
        if isinstance(obj, Decimal):
            return float(obj)
        raise TypeError(
            "Object of type '%s' is not JSON serializable" % type(obj).__name__
        )

    return json.dumps(obj, ensure_ascii=False, default=default)


def get_terminals(is_dict=False):
    ts = queries.get_terminals_query()
    # add is_hide to display on terminal panel
    for i in ts:
        i['is_hide'] = False

    return ts if is_dict else _dump_json(ts)


def get_heat_map(**kwargs):
    date_from = kwargs.get("date_from")
    date_to = kwargs.get("date_to")
    if not date_from or not date_to:
        log.warning("Heatmap dates filled incorrectly. Continue with defaults")
        date_from, date_to = "2017-01-01", "2018-09-01"
    result = queries.heatmap_query(date_from, date_to)
    return _dump_json(result)


def get_lines_by_filters(**kwargs):
    def get_ranges(a, b, parts=6):
        """Returns list of tuples (1, (10, 10_000))"""
        tmp = 10 ** round(math.log10(b) - 2)
        step = (b - a) / (parts + 1)
        borders = (
            [a] + [int(tmp * ((step * j) // tmp)) for j in range(1, parts)] + [b + 1]
        )
        return list(enumerate(zip(borders, borders[1:]), start=1))

    result = {}

    full_lines = queries.get_lines_by_filters(**kwargs)
    if full_lines:
        full_sum_value = sum([i['value'] for i in full_lines])
        lines = [i for i in full_lines if i['geojson'] is not None]
        max_value = float(max(lines, key=lambda x: x["value"])["value"])
        routing = [
            {"class": i, "range": [lb, ub], "sum_value": 0, "value": []}
            for (i, (lb, ub)) in get_ranges(0, max_value)
        ]

        for line in lines:
            for vrange in routing:
                if vrange["range"][0] < line["value"] <= vrange["range"][1]:
                    vrange["sum_value"] += line["value"]
                    vrange["value"].append(
                        {
                            "class": vrange["class"],
                            "geojson": line["geojson"],
                            "value": line["value"],
                        }
                    )

        result["routing"] = [rout for rout in routing if rout["sum_value"] > 0]

        codes = {code for row in lines for code in [row["p1"], row["p2"]]}
        stations = queries.get_stations_by_short_codes(codes)
        result["rail_station"] = {
            x["code"]: {
                "name": x["name"].capitalize(),
                "lat": x["lat"],
                "lon": x["lon"],
            }
            for x in stations
        }
        result["rail_station_by_name"] = {x["name"]: str(x["code"]) for x in stations}
        result["full_sum_value"] = full_sum_value

    return _dump_json(result)


def _get_pie_chart(**kwargs):
    def group_small_values(data, percent=0.15):
        max_val = float(max(filter(lambda x: x['name'], data), key=lambda x: x["y"])["y"])
        others = {"name": OTHERS, "y": 0}
        res = []
        for row in data:
            if float(row["y"]) / max_val <= percent:
                others["y"] += row["y"]
            else:
                res.append(row)
        if others["y"] > 0:
            res.append(others)
        return res

    data = queries.get_pie_chart(**kwargs)
    if data and len(data) >= 10: # if data contains more than 10 rows
        data = group_small_values(data, percent=0.15)
    for row in data:
        if not row['name']:
            row['name'] = ALL_DICTS_NONE
    return data


def _get_line_chart(**kwargs):
    if kwargs.get("chart_name") == "all":
        data = queries.get_all_chart(**kwargs)
    else:
        data = queries.get_line_chart(**kwargs)

    result = {}
    if data:
        unique_names = {row.get("name", ALL_DATA_CHART_NAME) for row in data}
        for name in unique_names:
            result[name] = []
        for row in data:
            name = row.get("name", ALL_DATA_CHART_NAME)
            result[name].append(
                {
                    "year": row.get("date").year,
                    "month": row.get("date").month,
                    "day": row.get("date").day,
                    "value": row.get("value"),
                }
            )
    for key in result:
        if not key:
            result[ALL_DICTS_NONE] = result.pop(key)
    return result


def get_charts(**kwargs):
    chart_name = kwargs.get("chart_name")
    if chart_name in CHARTS_LIST:
        if kwargs.get("chart_type") == "pie":
            data = _get_pie_chart(**kwargs)
        elif kwargs.get("chart_type") == "line":
            data = _get_line_chart(**kwargs)
        else:
            log.warning("Incorrect chart_type passed to get_charts method")
            pass

    result = {"name": CHARTS_LIST.get(chart_name), "data": data}
    return _dump_json(result)


@lru_cache(maxsize=32)
def get_all_dicts():
    rows = queries.get_all_dicts()
    all_dicts = dict(ALL_DICTS_LIST)
    for row in rows:
        for code in all_dicts:
            if code == row["name"]:
                row_id = row.get("id")
                row_value = row.get("value")
                if not row_value:
                    row_value = ALL_DICTS_NONE
                all_dicts[code]["vars"].update({row_id: row_value})

    return all_dicts


def save_filter(**kwargs):
    filter_data = _dump_json(kwargs.get("filter_data"))
    filter_name = kwargs.get("name")
    
    if len(queries.filter_get_id_by_name(filter_name)) > 0:
        return _dump_json({"error": f"Filter with name {filter_name} already exists"})
    queries.filter_insert_new(filter_name, filter_data)
    return _dump_json({"ok": "ok"})


def load_filters():
    data = queries.filter_load_all()
    return _dump_json({d["id"]: {"name": d["name"], "val": d["val"]} for d in data})


def del_filter(_id):
    return queries.filter_delete_by_id(_id)