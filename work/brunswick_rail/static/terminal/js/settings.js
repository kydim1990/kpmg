// main settings
var IS_INIT_MAP = true;
var IS_LOAD_MAP_TILELAYER = true;
var IS_INIT_HICHARTS = true;
var IS_USE_DEFAULT_CHART = true;

//additional settings
var IS_USE_HEAT_MAP = true;

// ---------Panel---------
var Filters = {};
var title_choose = 'Выберите значение';
var stations_short = {};
var LOADED_FILTER = {};
var IsFirst = true;
var DEFAULT_FILTERS;
var LIMIT_FILTER_VARS = 50;
var LIMIT_MULT = 1;
var FILTER_DICT;

// ---------Map---------
var map = null, debug = true, checkExist1;
var marker_is_clicked = false, isFirst = true;
var Layer_lines_group = L.layerGroup();
var Layer_stations_group = L.layerGroup();

var all_terminals_list = [];
var Layer_all_terminals = L.layerGroup();
var Layer_sea_terminals = L.layerGroup();
var Layer_railway_terminals = L.layerGroup();
var HEAT_MAP_LAYER;

var terminal_type_mode = {
    'all': 'Все',
    'sea': 'Морские',
    'rail': 'Сухопутные'
};

var MAP_LAEYRS_NAMES = {
    'simple': 'Обычный вид',
    'heat': 'Тепловая карта'
};

var SELECTED_OWNERS = [];

var RAINBOW = [
    'violet', 'indigo', 'blue', 'green', 'yellow', 'orange', 'red'
];
var ICON_SIZE = [20, 25];

//default date
var DT_FROM = new Date(2013, 0);
var DT_TO = new Date(2018, 7);

// ---------Charts---------
var CHART_COLORS = [
    '#00338D', '#0091DA', '#6D2077', '#005EB8', '#00A3A1',
    '#EAAA00', '#43B02A', '#C6007E', '#753F19', '#9B642E',
    '#9D9375', '#E3BC9F', '#E36877'
];


var CURRENT_CHART_CONFIG;

var Charts = {};
var ARROW_STEP = 350;
var CHART_TIMEOUT = 1000;
