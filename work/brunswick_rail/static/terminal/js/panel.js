$(document).ready(function () {
   // set_active_all_filters();
   init_vars();
   //
   init_filters();

   $('#indicator_list input[type="radio"]').change(function(e){
       let val = e.currentTarget.value;
       //change having
       let having = INDICATORS_DEFAULT_HAVING[val]['having'];

       $('#having').val(having);
       Filters['having'].val = having;
       Filters['indicator'].val = val;
    });

   init_date();
   bind_date();
});

var format_date = function date2str(x, y) {
    let z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
};

function init_vars() {
    // get all dicts
    $.ajax({
      type: 'POST',
      url: '/get_vars_front',
      contentType: "application/json; charset=utf-8",
      dataType: "json",

      success: function(d){
          //load filters
          FILTER_DICT = d.all_filters;

          //add additional fields for front
          $.each(FILTER_DICT, function (k, v) {
             FILTER_DICT[k].is_checked = true;
          });

      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log('Error init_vars', xhr.status, thrownError);
      }
    });
}

function init_date() {
    $('.date_tmpl .from').datepicker().data('datepicker').selectDate(DT_FROM);
    $('.date_tmpl .to').datepicker().data('datepicker').selectDate(DT_TO);
}


function bind_date() {
    $('.datepicker-here').datepicker({
        onSelect: function(formattedDate, date, inst) {
            let el = $(inst.el);
            let _id = el.data('id');
            let d_type = el.hasClass('from') ? 'from': 'to';
            let f_date = format_date(date, 'yyyy-MM-dd');
            Filters[_id].val[d_type] = f_date;
        }
    });
}

function click_menu_panel(el) {
    let elem = $(el);
    if (elem.hasClass('active'))
        return;

    let id = el.id;

    if (id === 'menu_calc'){
       //show calc
      $('#menu_calc').addClass('active');
      $('#menu_hist').removeClass('active');
      $('#menu_chart').removeClass('active');

      $('#hist_panel').addClass('hide');
      $('#chart_panel').addClass('hide');
      $('#calc_panel').removeClass('hide');
    }
    else if (id === 'menu_hist'){
       // show hist
       $('#menu_hist').addClass('active');
       $('#menu_calc').removeClass('active');
       $('#menu_chart').removeClass('active');

       $('#hist_panel').removeClass('hide');
       $('#calc_panel').addClass('hide');
       $('#chart_panel').addClass('hide');

       //load hist from server
       load_hist();
    }
    else if (id === 'menu_chart'){
       // show hist
       $('#menu_chart').addClass('active');
       $('#menu_calc').removeClass('active');
       $('#menu_hist').removeClass('active');

       $('#chart_panel').removeClass('hide');
       $('#calc_panel').addClass('hide');
       $('#hist_panel').addClass('hide');

    }

}

function init_filters() {
    //init dates

    $('#choose_filter').click(function () {
       if ($('#f_filter_list').hasClass('show')){
        //    debugger
       }
    });

    Filters['data_otpravlenija'] = {
        type: 'date',
        val: {
            from: moment(DT_FROM).format('YYYY-MM-DD'),
            to: moment(DT_TO).format('YYYY-MM-DD')
        }
    };


    $('.indicator input[value="sum_teu"]')[0].checked = true;
    $('#having').val(INDICATORS_DEFAULT_HAVING[DEFAULT_INDICATOR].having);

    Filters['indicator'] = {
      val: DEFAULT_INDICATOR
    };

    Filters['having'] = {
        val: INDICATORS_DEFAULT_HAVING[DEFAULT_INDICATOR].having
    };

    //
    if (IS_USE_DEFAULT_CHART){
        let title = get_default_indicator(DEFAULT_INDICATOR);

        get_charts_line({
            chart_name: 'all',
            chart_type: 'line',
            chart_y_title: title
        });
    }

    DEFAULT_FILTERS = Object.assign({}, Filters);
}

function get_default_indicator(k) {
    let get_indicator = INDICATOR_AS_JSON.find(function(element) {
      return element.code === k ;
    });

    return get_indicator.value;
}


function set_active_all_filters() {
    // get all dicts
    $.ajax({
      type: 'POST',
      url: '/get_all_filter_dicts',
      contentType: "application/json; charset=utf-8",
      dataType: "json",

      success: function(d){
          FILTER_DICT = d;

          //add additional fields for front
          $.each(FILTER_DICT, function (k, v) {
             FILTER_DICT[k].is_checked = true;
          });

      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log('Error get_routing', xhr.status, thrownError);
      }
    });
}


function add_filter(id, panel_id) {
    event.stopPropagation();
    let _id = $('#' + id);
    if (_id.hasClass('show')){
        console.log('removeClass show');
        _id.removeClass('show');
        $('#' + panel_id).removeClass('c_show');
        if (id=== 'f_filter_list1'){
            $('.map_select_terminals').removeClass('active');
        }
    }
    else{
        console.log('addClass show');
        _id.addClass('show');
        $('#' + panel_id).addClass('c_show');

        if (id === 'f_filter_list1'){
            $('.map_select_terminals').addClass('active');
        }

    }

    if (id !== 'f_filter_list'){
       if (!$('#filter_sub_panel.scroll_type_hide_2').length){
            $('#filter_sub_panel').addClass('scroll_type_hide_2');
        }
        else{
            $('#filter_sub_panel').removeClass('scroll_type_hide_2');
        }
    }

}

function hide_dropdown(id) {
    if (id == null){
        if (event.target.id === 'panel'){
            $('.filt_field').removeClass('show');
        }
    }
    else{
        $('#' + id).removeClass('show');
    }

}

function filterFunction(input, id) {
  let filter, a, i;
  filter = input.value.toUpperCase();
  let div = document.getElementById('f_' + id);
  a = div.getElementsByTagName("div");


  if (id !== 'filter_list' && id !== 'filter_list1' && Filters[id].is_limit){
     let list_vars = $(div).find('.list_vars')[0];
     let filter_res = {};

     $.each(FILTER_DICT[id].vars, function (k, v) {
         if (v.toUpperCase().indexOf(filter) > -1){
             filter_res[k] = v;
         }
     });

     let _multiply = id in filterFunction.multiply ? filterFunction.multiply[id] : 2;

     filter_res = get_limited_objects(id, filter_res, _multiply * LIMIT_FILTER_VARS);

     console.log('filter result:', filter_res);
     let html = terminal_templates.vars_list_template(filter_res);
     $(list_vars).html(html);

  }
  else{
      for (i = 0; i < a.length; i++) {
        if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
          $(a[i]).removeClass('hide');
        } else {
          $(a[i]).addClass('hide');
        }
      }
  }

}

filterFunction.multiply = {};

function etc_click(id) {
    let elem = $('#f_' + id + ' .search_inp')[0];
    filterFunction.multiply[id] = id in filterFunction.multiply ? filterFunction.multiply[id] += 1 : 2;
    elem.onkeyup.apply(elem, ['test', 'qwee']);
}

function remove_filter(id) {
    $('#' + id).remove();
    delete Filters[id];

    FILTER_DICT[id].is_checked = true;
    update_filter_list();
}

function update_filter_list() {
    let active_filters = {};
    $.each(FILTER_DICT, function (k, v) {
        if (v.is_checked){
            active_filters[k] = v;
        }
    });
    let html = terminal_templates.main_filter_template(active_filters);
    $('#d_filter_list').html(html);
}

function get_limited_objects(filt_id, obj, limit) {
    let is_used = function(k) {
        let k_int = parseInt(k);
        return filt_id in Filters && Filters[filt_id].val.indexOf(k_int) > -1;
    };

    //limit count list
    let vars = {}, is_limit;
    let vars_keys = Object.keys(obj);
    if (vars_keys.length > limit){
        let limit_vars = {};
        for (let i=0; i < limit; i++ ){
            limit_vars[vars_keys[i]] = {
                value: obj[vars_keys[i]],
                is_used: is_used(vars_keys[i])
            };
        }

        vars = limit_vars;
        is_limit = true
    }
    else{
        $.each(obj, function(k, v){
            vars[k] = {
                value: v,
                is_used: is_used(k)
            }
        });
    }
    return {
        vars: vars,
        is_limit: is_limit,
        id: filt_id
    }
}

function add_to_panel(id, v) {
    let html, d, vars_limited;

    //limit count list

    if (FILTER_DICT[id].type === 'date'){
        d = {id: id, name: v.name};
    }
    else{
        vars_limited = get_limited_objects(id, v.vars, LIMIT_FILTER_VARS);
        d = {id: id, name: v.name, vars: vars_limited.vars, is_limit: vars_limited.is_limit};
    }

    FILTER_DICT[id].is_checked = false;

    update_filter_list();

    if (v.type === 'date'){
        html = terminal_templates.filter_date_template(d);
        Filters[id] = {
            type: 'date',
            val: {
                from: moment(DT_FROM).format('YYYY-MM-DD'),
                to: moment(DT_TO).format('YYYY-MM-DD')
            }
        };
    }
    else if (v.type === 'list'){
        //add list
        html = terminal_templates.filter_list_template(d);
        Filters[id] = {
            type: 'list',
            val: [],
            is_limit: vars_limited.is_limit
        };
    }

    $(html).insertAfter($("#f_sub_f"));
    if (v.type === 'date') {
        //bind onselect
        let prefix = '#wrap_date_' + id;
        $(prefix + ' .from').datepicker().data('datepicker').selectDate(DT_FROM);
        $(prefix + ' .to').datepicker().data('datepicker').selectDate(DT_TO);
        bind_date();
    }

    $('#f_filter_list').removeClass('show');
}


function add_filter_elem(el) {
    let id = el.getAttribute('data-id');

    //add good codes if not exists
    let filt = FILTER_DICT[id];
    add_to_panel(id, filt);
    //hide main filter
    $('#add_filter').removeClass('c_show');
}

function onchange_date(id, from_to) {
    let v = event.target.value;
    if (!(id in Filters)){
        Filters[id] = {
            val: {
                [from_to]: v
            }
        }
    }
    else{
       Filters[id].val[from_to] = v;
    }

}


function onclick_filter_use(id) {
    let found = CHARTS_SELECTED.find(x => x.code === id);

    if (found.draw_chart === 1){
        //remove from CHARTS_SELECTED
        console.log('remove from CHARTS_SELECTED');
        found.draw_chart = 0;
        $('.chart_use[data-id="' + id + '"]').removeClass('active');
        //hide from result
        $('#pie_' + id).addClass('hide');
        $('#line_chart_' + id).addClass('hide');
    }
    else{
        if ($('#pie_' + id).length === 0){
            //download
            let html = terminal_templates.chart_panel_body_template([id]);
            let f_index = CHARTS_SELECTED.indexOf(found);

            let is_drown = CHARTS_SELECTED.filter(x => x.draw_chart === 1);
            let is_added = false;
            for (let i in is_drown){
                if (f_index < CHARTS_SELECTED.indexOf(is_drown[i])){
                    //add before if
                    let code_before_add = is_drown[i].code;
                    $(html).insertBefore($('#pie_' + code_before_add));
                    is_added = true;
                    break
                }
            }
            //if not found, then add to end
            if (!is_added){
                if (is_added){
                    let last_drown_code = is_drown[is_drown.length - 1].code;
                    $(html).insertAfter($('#line_chart_' + last_drown_code));
                }
                //add to
                else{
                    $('#map_chart_body').html(html);
                }

            }

        }
        else{
            //show
            $('#pie_' + id).removeClass('hide');
            $('#line_chart_' + id).removeClass('hide');
        }

        //if hiden default chart
        if ($('#line_chart_all.hide').length){
            //load charts
            get_charts_pie({
                chart_name: found.code,
                chart_type: 'pie'
            });

            let y_title = get_default_indicator(Filters.indicator.val);
            get_charts_line({
                chart_name: found.code,
                chart_type: 'line',
                chart_y_title: y_title
            });
        }

        //add to CHARTS_SELECTED
        console.log('add to CHARTS_SELECTED');
        found.draw_chart = 1;
        $('.chart_use[data-id="' + id + '"]').addClass('active');
        //hide from result
        //check if chart exists

    }

}

function update_title(filt_id) {
    let titles = [], title;
    if (Filters[filt_id].val.length === 0){
        title = title_choose;
        titles = [title];
    }
    else{
        $.each(Filters[filt_id].val, function (i, v) {
            titles.push(
                {
                    id: v,
                    name: FILTER_DICT[filt_id].vars[v]
                }
            );
        });
    }

    let config = {
        filt_id: filt_id,
        titles: titles,
    };
    let html = terminal_templates.filter_val_list_template(config);
    $('#b_' + filt_id).html(html);
}

function onchange_check() {
    let t = event.target;
    let filt_id = $(t).closest('div.filter_div')[0].id;
    let cleart_id;
    cleart_id = $(t).data('id');
    if (t.checked && Filters[filt_id].val.indexOf(cleart_id) < 0){
        //add to filter
        $(t).closest('div').addClass('checked_var');
        Filters[filt_id].val.push(cleart_id);

    }
    //delete from filter
    else{
        $(t).closest('div').removeClass('checked_var');
        let ind = Filters[filt_id].val.indexOf(cleart_id);
        Filters[filt_id].val.splice(ind, 1);
    }

    update_title(filt_id);
}

function filter_selected_click(id) {
    event.stopPropagation();
    let elem = $('#' + id);
    if (elem.length > 0){
        $(elem).click();
    }
    else{
        // delete only from Filters
        let _filter = event.target.getAttribute('data-filter');
        let _id = event.target.getAttribute('data-id');
        let ind = Filters[_filter].val.indexOf(parseInt(_id));
        Filters[_filter].val.splice(ind, 1);

        //delete from div
        update_title(_filter);
    }
}

function onchange_text_filt(id) {
    let v = event.target.value;
    Filters[id].val = v;
}

function hide_first_chart() {
   $('#line_chart_all').addClass('hide');
   $('.list_charts').removeClass('hide');
}

//clear
function clear_panel(is_load=false) {
    Filters = Object.assign({}, DEFAULT_FILTERS);
    init_filters();
    $('#filter_sub_panel').html('<div id="f_sub_f"></div>');

    $.each(FILTER_DICT, function (k, v) {
        v.is_checked = true;
    });

    update_filter_list();

    if (is_load){
        accept_filter();
    }
}

function show_first_chart() {
    $('#line_chart_all').removeClass('hide');
    $('.list_charts').addClass('hide');
}


// filter
function accept_filter() {
    clear_lines(true);
    clear_stations(true);
    console.log(Filters);

    add_lines(Filters);

    if (map.hasLayer(HEAT_MAP_LAYER)) {
        draw_heatmap();
    }
    
    hide_first_chart();

    let types = ['pie', 'line'];

    //draw charts
    setTimeout(
        function(){
             // hide not draw charts

             let CHARTS_SELECTED_KEYS = [];
             $.each(CHARTS_SELECTED, function (i, v) {
                 if (v.draw_chart === 1){
                    CHARTS_SELECTED_KEYS.push(v.code);
                 }
             });

             let html = terminal_templates.chart_panel_body_template(CHARTS_SELECTED_KEYS);
             $('#map_chart_body').html(html);
             //-
             //get tilte for chart
             let y_title = get_default_indicator(Filters.indicator.val);

             for (let i in types){
                for (let j in CHARTS_IDS){
                    //check if have to draw
                    if (CHARTS_SELECTED_KEYS.indexOf(CHARTS_IDS[j]) < 0){
                        continue
                    }

                    if (types[i] === 'pie'){
                        // pie
                        get_charts_pie({
                            chart_name: CHARTS_IDS[j],
                            chart_type: types[i]
                        });
                    }
                    else if (types[i] === 'line'){
                        //lines
                         get_charts_line({
                            chart_name: CHARTS_IDS[j],
                            chart_type: types[i],
                            chart_y_title: y_title
                         });
                    }
                }
             }
        },
        CHART_TIMEOUT
    );

    $('.map_chart_panel').scrollLeft(0);
}

