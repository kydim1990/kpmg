``// ------------------------------------
var LOADED_FILTER = {};

$(document).ready(function () {
    $('#save_templ_as').on('shown.bs.modal', function () {
        $('#save_name_filter').focus();
    });
});

function save_filter() {
    let name = $('#save_name_filter').val();
    $('#hist_exists').addClass('hide');
    let data = {name: name, filter_data: Filters};
    console.log(data);
    $.ajax({
      type: 'POST',
      url: '/save_filter',
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: JSON.stringify(data),

      success: function(d){
         console.log('save_filter', d);
         let d_parse = d;
         if (Object.keys(d_parse).indexOf('error') > -1){
             $('#hist_exists').html(d_parse.error);
             $('#hist_exists').removeClass('hide');
         }
         else{
            $('#save_templ_as').modal('hide');
            $('#save_name_filter').val(' ');
         }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log('Error save_filter', xhr.status, thrownError);
      }
    });
}

function load_hist() {
    LOADED_FILTER = {};
    $('#list_hist').html(' ');

    $.ajax({
      type: 'POST',
      url: '/load_filters',

      success: function(d){
         console.log('load_hist', d);

         $.each(d, function (k, v) {
             LOADED_FILTER[k] = {
                 name: v.name,
                 val: JSON.parse(v.val)
             }
         });

         //
         let html = terminal_templates.list_hist_template(LOADED_FILTER);
         $('#list_hist').html(html);

      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log('Error load_filters', xhr.status, thrownError);
      }
    });

}

function set_data(id, data) {
    // --
    if (id === 'indicator'){
        $("#indicator_list :radio[value=" + data.val + "]").prop('checked', true);
    }
    else if (id === 'having'){
        $('#having').val(data.val);
    }
    else if (id === 'data_otpravlenija'){
        $('#data_otp_from').val(data.val.from);
        $('#data_otp_to').val(data.val.to);
    }
    // --common--
    else {
        let data_full = FILTER_DICT[id];
        add_to_panel(id, data_full);
        if (data.type === 'date'){
            $('#' + id + '_from').datepicker().data('datepicker').selectDate(new Date(data.val.from));
            $('#' + id + '_to').datepicker().data('datepicker').selectDate(new Date(data.val.to));
        }
        else if (data.type === 'list'){
            //set label
            let names = data.val.map(x => data_full.vars[x]);
            let title = names.join();
            $('#b_' + id).text(title);

            //set checkbox
            $.each(data.val, function (i, v) {
                document.getElementById(id + "_" + v).checked = true;
            })
        }
    }


    // ----------------------------------
    if (id in Filters){
        Filters[id].val = data.val;
    }
    else{
        Filters[id] = {
            type: data.type,
            val: data.val
        }
    }

    $('#menu_calc').click();
}

function load_one_hist(hist_id) {
    clear_panel(false);

    let load_hist = hist_id;
    let f_new = LOADED_FILTER[load_hist].val, _id;
    let indic_hav = ['indicator', 'having', 'data_otpravlenija'];
    //set data_otpravlenija //set indicator
    for (let i in indic_hav){
        _id = indic_hav[i];
        set_data(_id, f_new[_id]);
    }

    $.each(f_new, function (k, v) {
       if (indic_hav.indexOf(k) === -1){
           console.log(k);
           set_data(k, v);
       }

    });
    console.log('load hist', load_hist);
    console.log('filter new', f_new);

    accept_filter();
}


function del_hist(hist_id) {
    $.ajax({
      type: 'GET',
      url: '/del_hist/' + hist_id,

      success: function(d){
          if (d > 0){
               console.log(d);
               //remove
              $('#row_hist_' + hist_id).remove();
          }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log('Error save_filter', xhr.status, thrownError);
      }
    });
}

function search_hist_key_up() {
    let val = event.target.value;
    $.each($('#list_hist .row_hist'), function (i, v) {
        if (v.getAttribute('data-value').indexOf(val) > -1){
            $(v).removeClass('hide');
        }
        else{
            $(v).addClass('hide');
        }
    });
}