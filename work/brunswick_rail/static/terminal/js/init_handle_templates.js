//init_handle_templates.js
var terminal_templates = {};

$(document).ready(function () {
    init_tempalte();
    handlebars_helpers();
});

function init_tempalte() {
    let props = {
        'description': {is_sub_templ: false},
        'list': {is_sub_templ: false},
        'filter': {is_sub_templ: false},
        'list_routing': {is_sub_templ: false},
        'stations': {is_sub_templ: false},
        'filter_date': {is_sub_templ: false},
        'filter_list': {is_sub_templ: false},
        'filter_text': {is_sub_templ: false},
        'main_filter': {is_sub_templ: false},
        'list_hist': {is_sub_templ: false},
        'legends': {is_sub_templ: false},
        'select_terminals': {is_sub_templ: false},
        'bind_with_stations': {is_sub_templ: false},
        'chart_panel': {is_sub_templ: false},
        'chart_full_size': {is_sub_templ: false},
        'layer_list': {is_sub_templ: false},
        'vars_list': {is_sub_templ: true},
        'chart_panel_body': {is_sub_templ: true},
        'filter_val_list': {is_sub_templ: false}
    };

    let name, fullname;
    $.each(props, function (k, v) {
        name = k;
        fullname = "terminal_" + name + "-template";
        terminal_templates[name] = $('#' + fullname).html();
        if (v.is_sub_templ){
            Handlebars.registerPartial(fullname, terminal_templates[name]);
        }
        
        terminal_templates[name + "_template"] = Handlebars.compile(terminal_templates[name]);
    });

}

function handlebars_helpers() {
    Handlebars.registerHelper('ifeq', function (a, b, options) {
        if (a == b) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
}
