import datetime
import json
from unittest import mock

import pytest

from terminal import utils
from terminal.config import ALL_DATA_CHART_NAME, CHARTS_LIST
from terminal.database import DatabaseQueries


class TestCharts:
    charts_ids = [i for i in CHARTS_LIST if i != "all"]
    named_charts = [(i, CHARTS_LIST[i]) for i in charts_ids]

    @pytest.mark.parametrize("code,name", named_charts, ids=charts_ids)
    def test_get_pie_chart(self, code, name, monkeypatch):
        db_data = [{"name": "a", "y": 100}, {"name": "b", "y": 200}]
        with monkeypatch.context() as monkey:
            m = mock.MagicMock(return_value=db_data)
            monkey.setattr(DatabaseQueries, "get_pie_chart", m)
            res = utils.get_charts(chart_name=code, chart_type="pie")
            m.assert_called_once_with(chart_name=code, chart_type="pie")
        expected = {"name": name, "data": db_data}
        assert expected == json.loads(res)

    @pytest.mark.parametrize("code,name", named_charts, ids=charts_ids)
    def test_get_line_chart(self, code, name, monkeypatch):
        db_data = [
            {"name": "a", "date": datetime.datetime(2013, 1, 1), "value": 100},
            {"name": "b", "date": datetime.datetime(2013, 1, 1), "value": 100},
        ]

        with monkeypatch.context() as monkey:
            m = mock.MagicMock(return_value=db_data)
            monkey.setattr(DatabaseQueries, "get_line_chart", m)
            res = utils.get_charts(chart_name=code, chart_type="line")
            m.assert_called_once_with(chart_name=code, chart_type="line")

        expected = {
            "data": {
                "a": [{"year": 2013, "month": 1, "day": 1, "value": 100}],
                "b": [{"year": 2013, "month": 1, "day": 1, "value": 100}],
            },
            "name": name,
        }
        assert expected == json.loads(res)

    def test_get_all_chart(self, monkeypatch):
        db_data = [{"date": datetime.datetime(2013, 1, 1), "value": 100}]

        with monkeypatch.context() as monkey:
            m = mock.MagicMock(return_value=db_data)
            monkey.setattr(DatabaseQueries, "get_all_chart", m)
            res = utils.get_charts(chart_name="all", chart_type="line")
            m.assert_called_once_with(chart_name="all", chart_type="line")

        expected = {
            "name": ALL_DATA_CHART_NAME,
            "data": {
                ALL_DATA_CHART_NAME: [
                    {"year": 2013, "month": 1, "day": 1, "value": 100}
                ]
            },
        }

        assert expected == json.loads(res)
