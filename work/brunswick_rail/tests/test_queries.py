from unittest import mock

import pytest

from terminal.database import DatabaseQueries, DatabaseWrapper


class TestDatabaseQueries:
    params = {
        "indicator": {"val": "sum_teu"},
        "having": {"val": 1000},
        "data_otpravlenija": {"val": {"from": "2013-01-01", "to": "2018-09-01"}},
    }

    @pytest.fixture(autouse=True)
    def exec_mock(self, monkeypatch):
        m = mock.MagicMock(return_value=[])
        monkeypatch.setattr(DatabaseWrapper, "execute", m)
        return m

    @pytest.fixture
    def queries(self):
        return DatabaseQueries(None)

    @pytest.fixture
    def templates(self):
        return DatabaseQueries(None).load_templates()

    def test_get_all_chart(self, exec_mock, queries, templates):
        target = templates.get("all_stat").format(
            indicator=self.params["indicator"]["val"],
            having=self.params["having"]["val"],
            date_from=self.params["data_otpravlenija"]["val"]["from"],
            date_to=self.params["data_otpravlenija"]["val"]["to"],
        )
        assert [] == queries.get_all_chart(**self.params)
        exec_mock.assert_called_once_with(target)
