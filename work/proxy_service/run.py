from os import environ
from app import application

if __name__ == '__main__':
    try:
        HOST = environ.get('SERVER_HOST', '0.0.0.0')
        PORT = int(environ.get('SERVER_PORT', '5555'))
    except AttributeError:
        HOST = '0.0.0.0'
        PORT = 5555
    application.run(HOST, PORT, application.config['DEBUG'])
