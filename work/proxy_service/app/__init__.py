from flask import Flask
from logging.config import dictConfig
from app import config
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask_sqlalchemy import SQLAlchemy

application = Flask(__name__)

application.config.from_object(config)
db = SQLAlchemy(application)

from app import views
