from app.models import ProxyList, ProxyBlackList
from flask import jsonify
from datetime import datetime
from time import sleep
from app import db
import requests


def fget_free_proxy() -> dict:
    proxy = ProxyList.query.filter(ProxyList.last_used == None).first()
    if proxy is None:
        proxy = ProxyList.query.order_by(ProxyList.last_used).first()
    if proxy is None:
        return jsonify(None)

    if proxy.last_used is not None:
        delta_seconds = (datetime.now() - proxy.last_used).seconds
        if delta_seconds < 10:
            print(f'sleeping {delta_seconds} seconds')
            sleep(delta_seconds)

    url_ = proxy.url
    type_ = proxy.type

    proxies = {
        type_: f'{type_}://{url_}'
    }
    proxy.last_used = datetime.now()
    db.session.merge(proxy)
    db.session.commit()

    return proxies


def fget_use_proxy(url: str, method: str='GET', timeout: int=5, post_data: dict=None):
    def del_proxy(proxies_):
        k_ = list(proxies_.keys())[0]
        ip_ = proxies_[k_].split('://')[1]
        print(f'delete proxy: {ip_}')
        fdelete_proxy(ip_)

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                      ' Chrome/32.0.1667.0 Safari/537.36'
    }

    max_trying = 10
    while True and max_trying > 0:
        try:
            proxies = fget_free_proxy()
            print(f'use proxies= {proxies}')
            kwargs = {
                'proxies': proxies,
                'headers': headers,
                'timeout': timeout
            }
            if method == 'GET':
                print(f'GET: {url}, kwargs: {kwargs}')
                r = requests.get(url, **kwargs)
            else:
                print(f'POST: {url}, data: {post_data},  kwargs: {kwargs}')
                r = requests.post(url=url, data=post_data, **kwargs)
            print(f'status code: {r.status_code}')
            if r.status_code != 200:
                print(r.reason)
                del_proxy(proxies)
                continue

            return r

        except TypeError as e:
            print(e)
        except Exception as e:
            print(str(e))
            if proxies.keys() is None:
                return
            print(str(e))
            if proxies.keys() is None:
                return

            del_proxy(proxies)
        max_trying -= 1
    print('Exit with max try')


def fdelete_proxy(del_proxy: str):
    pd = ProxyList.query.filter_by(url=del_proxy).first()
    db.session.delete(pd)
    pb = ProxyBlackList(url=del_proxy)
    db.session.add(pb)
    db.session.commit()


def fget_list_proxies(num: int=100):
    proxy_list = [p.url for i, p in enumerate(ProxyList.query.all()) if i < num]
    return proxy_list


def fget_count_proxy():
    count = ProxyList.query.count()
    return count


if __name__ == '__main__':
    a = fget_list_proxies()
    print(a)
