from app import db
from datetime import datetime, timedelta
from sqlalchemy.orm import relationship
from sqlalchemy import inspect


class ProxyList(db.Model):
    __tablename__ = 'proxy_list'
    url = db.Column(db.String, primary_key=True)
    type = db.Column(db.String)
    delay = db.Column(db.Integer)
    date_created = db.Column(db.TIMESTAMP, default=datetime.now())
    last_used = db.Column(db.TIMESTAMP)
    last_request_url = db.Column(db.String)


class ProxyBlackList(db.Model):
    __tablename__ = 'proxy_black_list'
    url = db.Column(db.String, primary_key=True)
