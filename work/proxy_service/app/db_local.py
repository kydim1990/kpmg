import os
from sqlalchemy import Column, ForeignKey, Integer, String, TEXT, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from datetime import datetime

database_file = os.path.join(os.path.dirname(__file__), 'data.db')
engine = create_engine('sqlite:///%s' % database_file)
db_session = scoped_session(sessionmaker(autocommit=True,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


class Stats(Base):
    __tablename__ = 'stats'
    # id = Column(Integer, primary_key=True, autoincrement=True)
    code = Column(String, primary_key=True)
    data = Column(TEXT)
    ts = Column(DateTime, default=datetime.now())


if __name__ == '__main__':
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
