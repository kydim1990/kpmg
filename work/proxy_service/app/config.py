from os import environ
DB_PORT = environ.get('DB_PORT', 5432)
DB_HOST = environ.get('DB_HOST', 'localhost')

DEBUG = environ.get("DEBUG", True)
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_DATABASE_URI = f"postgresql+psycopg2://postgres:postgres@{DB_HOST}:{DB_PORT}/postgres"
