from db_local import session, Stats
from config import URL_LEAGUE_TABLE, URL_FULL_STATISTIC, URL_FIXTURES, TOKENS, SLEEP_TIME
import requests
import logging
import os
import json
from time import sleep
import sys
from datetime import datetime


sleep_time = 300
log_name = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'cron_db.log')

logging.basicConfig(filename=log_name, level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

CODES = {
    'LEAGUE_TABLE': URL_LEAGUE_TABLE,
    'FIXTURES': URL_FIXTURES,
    'FULL_STATISTIC': URL_FULL_STATISTIC,
}

LIMIT_WAIT = 5


def get_request(url, name=None):
    has_error = True
    i = 0
    try:
        # proxies = {
        #     'http': 'http://103.78.213.147:80',
        #     'https': 'http://103.78.213.147:80',
        # }
        while has_error and i < len(TOKENS):
            headers = {'X-Auth-Token': TOKENS[i], 'X-Response-Control': 'minified'}
            print('url= {0}'.format(url))
            print('Token= {0}'.format(TOKENS[i]))
            # r = requests.get(url, headers=headers, proxies=proxies)
            r = requests.get(url, headers=headers)
            if (name == 'FULL_STATISTIC' and r.reason == 'Too Many Requests') \
                    or 'error' in json.loads(r.text):
                wait = True
                i = 0
                while wait and i < LIMIT_WAIT:
                    print('wait 60 sec, reason {0}'.format(r.reason))
                    sleep(60)
                    print('try {0}'.format(i))
                    r = requests.get(url, headers=headers)
                    wait = r.reason == 'Too Many Requests'
                    i += 1
            # print(r.status_code)
            data = json.loads(r.text)
            # data = r.json()
            if not ('error' in data):
                return r.text
            i += 1
    except Exception as e:
        print(str(e))


def get_request_teams(not_save=False):
    for k, v in CODES.items():
        try:
            print('Use {0}'.format(k))
            data = get_request(v, name=k)
            if data is None:
                log = 'request return None'
                print(log)
                continue
            log = '{0}: Len of new data = {1}'.format(k, len(data))
            print(log)
            logging.info(log)
            if data is not None:
                a = Stats(code=k, data=data, ts=datetime.now())
                session.merge(a)
                session.commit()
                print(' commit')
                logging.info('commit')
        except Exception as e:
            print(str(e))


if __name__ == '__main__':
    get_request_teams()


