# -*- coding: latin-1 -*-

from app import application
from flask import jsonify, request
from app.utils import fget_free_proxy, fdelete_proxy, fget_use_proxy, fget_list_proxies, fget_count_proxy


@application.route('/', methods=['GET', 'POST'])
def index():
   if request.method == 'GET':
      res = 'get ok'
   elif request.method == 'POST':
      res = 'post ok'
   return jsonify(res)


@application.route('/get_free_proxy')
def get_free_proxy():
    proxies = fget_free_proxy()
    return jsonify(proxies)


@application.route('/get_list_proxies')
@application.route('/get_list_proxies/<int:num>')
def get_list_proxies(num: int=100):
    lp = fget_list_proxies(num)
    res = {
        'count': len(lp),
        'urls': lp
    }
    return jsonify(res)


@application.route('/delete_proxy/<del_proxy>')
def delete_proxy(del_proxy: str):
    fdelete_proxy(del_proxy)
    return jsonify(True)


@application.route('/count_proxy')
def count_proxy():
    res = fget_count_proxy()
    return jsonify({
        'count': res
    })


@application.route('/get_use_proxy', methods=['POST'])
def get_use_proxy():
    content = request.json
    if 'url' in content:
        url = content['url']
        kwargs = {
            "method": content.get("method", None),
            "post_data": content.get("post_data", None),
        }
        r = fget_use_proxy(url, **kwargs)
        if 'gettype' in content:
            type_ = content['gettype']
            if type_ == 'json':
                res = r.json()
                return jsonify(res)

        res = {
            'html': r.text
        }
        return jsonify(res)
    else:
        return jsonify({
            'error': 'Not found url parameter'
        })

