#Proxy server
##Provide tool to get GET and POST request using proxy

### Get count proxy
``
GET http://<host>:<port>/count_proxy
``

### Get free proxy
``
GET http://<host>:<port>/get_free_proxy
``


### Get list proxy (first 100 rows)
``
GET http://<host>:<port>/get_list_proxies
``

### Delete proxy
``
GET http://<host>:<port>/delete_proxy/<proxy_to_delete>
``

### send request use proxy
#### gettype: {json}, default content
#### method: {GET, POST}, default GET
#### post_data: data to sen POST request

``
POST http://<host>:<port>/get_use_proxy
Content-Type: application/json

{
    "url": "https://api.ipify.org?format=json",
    "method": "GET",
    "post_data": {},
    "gettype": "json"
}
``
