from os import environ

DEBUG = environ.get('DEBUG', True)

DB_PORT = environ.get('DB_PORT', 5432)
DB_HOST = environ.get('DB_HOST', 'localhost')


PROXY_URLS = {
    # 'http://spys.one/proxies/': 'use_spys',
    'http://www.gatherproxy.com/': 'use_gatherproxy',
    'https://www.proxynova.com/proxy-server-list/': 'use_proxynova'
}

DB_URL = f"postgresql+psycopg2://postgres:postgres@{DB_HOST}:{DB_PORT}/postgres"
DELAY = 0.5
