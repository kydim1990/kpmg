from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, Column, Integer, String, TIMESTAMP
from config import DB_URL
from datetime import datetime

engine = create_engine(DB_URL)
Base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session()


class ProxyList(Base):
    __tablename__ = 'proxy_list'
    url = Column(String, primary_key=True)
    type = Column(String)
    delay = Column(Integer)
    date_created = Column(TIMESTAMP, default=datetime.now())
    last_used = Column(TIMESTAMP)
    last_request_url = Column(String)


class ProxyBlackList(Base):
    __tablename__ = 'proxy_black_list'
    url = Column(String, primary_key=True)


if __name__ == '__main__':
    a = session.query(ProxyList).all()
    p = ProxyList(type='tets_type', url='test_url11', date_created=datetime.now(),
                  last_request_url='last_request_url')
    session.merge(p)
    session.commit()
    b = 1
