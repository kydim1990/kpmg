import requests
from time import sleep
from bs4 import BeautifulSoup
from config import PROXY_URLS, DELAY
from functools import lru_cache
from sqlalchemy import create_engine
from models import ProxyList, session, ProxyBlackList
from requests.adapters import HTTPAdapter
import re
import json
from requests_html import HTMLSession


class Writer(object):
    def __init__(self):
        self.user_agent = 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)' \
                          ' Chrome/32.0.1667.0 Safari/537.36'
        self.headers = {
            'User-Agent': self.user_agent
        }

    def check_proxy(self, proxy):
        url_check = 'https://api.ipify.org?format=json'
        type_ = proxy['type']
        url_ = proxy['url']
        print(f'checking {url_} ...')
        #check black list
        bl_check = session.query(ProxyBlackList).filter(ProxyBlackList.url == url_).first()
        if bl_check is not None:
            print(f'{url_} in black list')
            return False

        #check format url
        if ':' not in url_:
            print('bad format')
            return False

        proxies = {
            type_: f'{type_}://{url_}'
        }
        try:
            r = requests.get(url_check, proxies=proxies, headers=self.headers, timeout=1)
            if r.status_code == 200:
                print(f'{url_} - True')
                return True
            else:
                print(f'{url_} - False status response')
                return False
        except Exception as e:
            # print(str(e))
            print(f'{url_} - False')
            return False

    def write_in_db(self, p_list):
        print(p_list)
        for j in p_list:
            try:
                print(f"ProxyList(url={j['url']}, type={j['type']}, delay={j['delay']}")
                p = ProxyList(url=j['url'], type=j['type'], delay=j['delay'])
                print(f'add {p}')
                session.merge(p)
            except Exception as e:
                print('Error write_in_db')
                print(str(e))
        session.commit()
        print('commit')

    # for http://spys.one/proxies/
    def use_spys(self, *args):
        session_use_spys = HTMLSession()

        @lru_cache(maxsize=None)
        def get_url(t):
            try:
                w = t.select_one('font.spy14')
                res = w.text.split('document')[0]
                return res
            except Exception as e:
                print(e)
                return None

        @lru_cache(maxsize=None)
        def get_type(t):
            res = t.select_one('font.spy1').text
            if '(' in res:
                res = res.split('(')[0].strip()
            return res

        @lru_cache(maxsize=None)
        def get_delay(t):
            res = float(t.select_one('font.spy1').text)
            return res

        @lru_cache(maxsize=None)
        def get_proxies(soup):
            spy1xx = [row for i, row in enumerate(soup.select('.spy1xx')) if i > 0]
            # print(f'spy1xx: {spy1xx}')
            spy1x = [row for i, row in enumerate(soup.select('.spy1x')) if i > 0]
            # print(f'spy1xx: {spy1x}')
            spy_rows = spy1xx + spy1x
            rows = [td.find_all('td') for i, td in enumerate(spy_rows)]
            rows_for_db = [
                {
                    'url': get_url(r[0]),
                    'type': get_type(r[1]).lower(),
                    'delay': get_delay(r[3])
                }
                for r in rows if get_url(r[0]) is not None and get_delay(r[3]) < DELAY
            ]

            if rows_for_db:
                p_list = [j for j in rows_for_db if self.check_proxy(j)]
                self.write_in_db(p_list)
                # print(p_list)
                # for j in p_list:
                #     try:
                #         print(f"ProxyList(url={j['url']}, type={j['type']}, delay={j['delay']}")
                #         p = ProxyList(url=j['url'], type=j['type'], delay=j['delay'])
                #         print(f'add {p}')
                #         session.merge(p)
                #     except Exception as e:
                #         print('Error')
                #         print(str(e))
                # session.commit()
                # print('commit')

        print('use_spys')
        url = args[0]
        print(f'url: {url}')
        # r = requests.get(url, headers=self.headers)
        r = session_use_spys.get(url, headers=self.headers)
        html = r.html.html
        soup_first = BeautifulSoup(html, 'html.parser')
        next_urls = {
            'http://spys.one{0}'.format(u.get('href'))
            for i, u in enumerate(soup_first.select('.spy1xx a')) if i > 0
        } | {None}

        for i in next_urls:
            if i is not None:
                # r = requests.get(i, headers=self.headers)
                r = session_use_spys.get(i, headers=self.headers)
                soup = BeautifulSoup(r.content, 'html.parser')
            else:
                soup = soup_first
            get_proxies(soup=soup)

    def use_gatherproxy(self, *args):
        print('use_gatherproxy')
        url = args[0]
        print(f'url: {url}')
        r = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(r.content, 'html.parser')
        table = soup.select_one('table')
        str_t = str(table)
        p_list = re.findall(r'\((.*)\)', str_t)
        proxy_list = [json.loads(i) for i in p_list if 'PROXY_IP' in i]

        proxy_list = [
            {
                'url': f"{i['PROXY_IP']}:{int(i['PROXY_PORT'], 16)}",
                'type': 'http',
                'delay': int(i["PROXY_TIME"])
            }
            for i in proxy_list if i["PROXY_STATUS"] == 'OK' and int(i["PROXY_TIME"]) < 100
        ]

        proxy_list = [i for i in proxy_list if self.check_proxy(i)]
        if proxy_list:
            self.write_in_db(proxy_list)

    def use_proxynova(self, *args):

        @lru_cache(maxsize=None)
        def get_ip(ip_):
            if ip_.get('title').replace('.', '').isdigit():
                return ip_.get('title')
            else:
                str_ip = str(ip_)
                try:
                    w = re.findall(r'\((.*)\)', str_ip)[0]
                    w1 = w.replace('.substr(8)', '[8:]')
                    res = eval(w1)
                    return res
                except Exception as e:
                    print(e)
                    return None

        @lru_cache(maxsize=None)
        def get_port(tag):
            port_tag = tag.select_one('a[href^="/proxy-server-list/port-"]')
            if port_tag is not None:
                return port_tag.text

        @lru_cache(maxsize=None)
        def get_speed(tag):
            s = tag.find('small')
            if s is None:
                return
            s = s.text.replace('ms', '').strip()
            if s.isdigit():
                return int(s)

        print('use_gatherproxy')
        url = args[0]
        print(f'url: {url}')
        r = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(r.content, 'html.parser')
        trs = soup.select('table tbody tr')
        p_list = [
            {
                'url': '{0}:{1}'.format(get_ip(t.find('abbr')), get_port(t)),
                'type': 'http',
                'delay': 0.5
            }
            for i, t in enumerate(trs) if t.find('abbr') is not None and get_port(t) is not None and get_speed(t) > 2000
        ]
        proxy_list = [i for i in p_list if self.check_proxy(i)]
        if proxy_list:
            self.write_in_db(proxy_list)

    def run(self):
        for k, v in PROXY_URLS.items():
            func = getattr(self, v)
            func(k)


def main():
    w = Writer()
    while True:
        try:
            w.run()
            print('sleeping....')
            sleep(300)
        except Exception as e:
            print(e)


if __name__ == '__main__':
    main()
