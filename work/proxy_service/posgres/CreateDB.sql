CREATE TABLE proxy_list (
    url character varying(100) PRIMARY KEY ,
    type character varying(10) NOT NULL,
    delay INTEGER,
    date_created timestamp,
    last_used timestamp,
    last_request_url character varying(100)
);

CREATE TABLE proxy_black_list (
    url character varying(100) PRIMARY KEY
);